import NavbarItem from 'app/layouts/navbar/navbar-item.model';

export const EntityNavbarItems: NavbarItem[] = [
  {
    name: 'Archivierte',
    route: '/archived-items',
    translationKey: 'Test',
  },
];
