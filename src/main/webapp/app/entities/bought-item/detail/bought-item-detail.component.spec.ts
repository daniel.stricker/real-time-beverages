import { ComponentFixture, TestBed } from '@angular/core/testing';
import { provideRouter, withComponentInputBinding } from '@angular/router';
import { RouterTestingHarness } from '@angular/router/testing';
import { of } from 'rxjs';

import { BoughtItemDetailComponent } from './bought-item-detail.component';

describe('BoughtItem Management Detail Component', () => {
  let comp: BoughtItemDetailComponent;
  let fixture: ComponentFixture<BoughtItemDetailComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [BoughtItemDetailComponent],
      providers: [
        provideRouter(
          [
            {
              path: '**',
              component: BoughtItemDetailComponent,
              resolve: { boughtItem: () => of({ id: 123 }) },
            },
          ],
          withComponentInputBinding(),
        ),
      ],
    })
      .overrideTemplate(BoughtItemDetailComponent, '')
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BoughtItemDetailComponent);
    comp = fixture.componentInstance;
  });

  describe('OnInit', () => {
    it('Should load boughtItem on init', async () => {
      const harness = await RouterTestingHarness.create();
      const instance = await harness.navigateByUrl('/', BoughtItemDetailComponent);

      // THEN
      expect(instance.boughtItem()).toEqual(expect.objectContaining({ id: 123 }));
    });
  });

  describe('PreviousState', () => {
    it('Should navigate to previous state', () => {
      jest.spyOn(window.history, 'back');
      comp.previousState();
      expect(window.history.back).toHaveBeenCalled();
    });
  });
});
