import { Component, inject, input } from '@angular/core';
import { RouterModule } from '@angular/router';

import SharedModule from 'app/shared/shared.module';
import { DurationPipe, FormatMediumDatePipe, FormatMediumDatetimePipe } from 'app/shared/date';
import { IBoughtItem } from '../bought-item.model';
import { DataUtils } from '../../../core/util/data-util.service';

@Component({
  standalone: true,
  selector: 'jhi-bought-item-detail',
  templateUrl: './bought-item-detail.component.html',
  imports: [SharedModule, RouterModule, DurationPipe, FormatMediumDatetimePipe, FormatMediumDatePipe],
})
export class BoughtItemDetailComponent {
  boughtItem = input<IBoughtItem | null>(null);
  protected dataUtils = inject(DataUtils);

  previousState(): void {
    window.history.back();
  }

  byteSize(base64String: string): string {
    return this.dataUtils.byteSize(base64String);
  }

  openFile(base64String: string, contentType: string | null | undefined): void {
    this.dataUtils.openFile(base64String, contentType);
  }
}
