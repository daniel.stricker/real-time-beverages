import { Injectable } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';

import dayjs from 'dayjs/esm';
import { DATE_TIME_FORMAT } from 'app/config/input.constants';
import { IBoughtItem, NewBoughtItem } from '../bought-item.model';

/**
 * A partial Type with required key is used as form input.
 */
type PartialWithRequiredKeyOf<T extends { id: unknown }> = Partial<Omit<T, 'id'>> & { id: T['id'] };

/**
 * Type for createFormGroup and resetForm argument.
 * It accepts IBoughtItem for edit and NewBoughtItemFormGroupInput for create.
 */
type BoughtItemFormGroupInput = IBoughtItem | PartialWithRequiredKeyOf<NewBoughtItem>;

/**
 * Type that converts some properties for forms.
 */
type FormValueOf<T extends IBoughtItem | NewBoughtItem> = Omit<T, 'boughtTime' | 'billingTime' | 'paidTime'> & {
  boughtTime?: string | null;
  billingTime?: string | null;
  paidTime?: string | null;
};

type BoughtItemFormRawValue = FormValueOf<IBoughtItem>;

type NewBoughtItemFormRawValue = FormValueOf<NewBoughtItem>;

type BoughtItemFormDefaults = Pick<NewBoughtItem, 'id' | 'boughtTime' | 'billingTime' | 'paidTime'>;

type BoughtItemFormGroupContent = {
  id: FormControl<BoughtItemFormRawValue['id'] | NewBoughtItem['id']>;
  name: FormControl<BoughtItemFormRawValue['name']>;
  price: FormControl<BoughtItemFormRawValue['price']>;
  boughtTime: FormControl<BoughtItemFormRawValue['boughtTime']>;
  billingTime: FormControl<BoughtItemFormRawValue['billingTime']>;
  paidTime: FormControl<BoughtItemFormRawValue['paidTime']>;
  amount: FormControl<BoughtItemFormRawValue['amount']>;
  offer: FormControl<BoughtItemFormRawValue['offer']>;
  buyer: FormControl<BoughtItemFormRawValue['buyer']>;
};

export type BoughtItemFormGroup = FormGroup<BoughtItemFormGroupContent>;

@Injectable({ providedIn: 'root' })
export class BoughtItemFormService {
  createBoughtItemFormGroup(boughtItem: BoughtItemFormGroupInput = { id: null }): BoughtItemFormGroup {
    const boughtItemRawValue = this.convertBoughtItemToBoughtItemRawValue({
      ...this.getFormDefaults(),
      ...boughtItem,
    });
    return new FormGroup<BoughtItemFormGroupContent>({
      id: new FormControl(
        { value: boughtItemRawValue.id, disabled: true },
        {
          nonNullable: true,
          validators: [Validators.required],
        },
      ),
      name: new FormControl(boughtItemRawValue.name, {
        validators: [Validators.required, Validators.minLength(1)],
      }),
      price: new FormControl(boughtItemRawValue.price, {
        validators: [Validators.required, Validators.min(0)],
      }),
      boughtTime: new FormControl(boughtItemRawValue.boughtTime, {
        validators: [Validators.required],
      }),
      billingTime: new FormControl(boughtItemRawValue.billingTime),
      paidTime: new FormControl(boughtItemRawValue.paidTime),
      amount: new FormControl(boughtItemRawValue.amount, {
        validators: [Validators.required, Validators.min(1)],
      }),
      offer: new FormControl(boughtItemRawValue.offer, {
        validators: [Validators.required],
      }),
      buyer: new FormControl(boughtItemRawValue.buyer, {
        validators: [Validators.required],
      }),
    });
  }

  getBoughtItem(form: BoughtItemFormGroup): IBoughtItem | NewBoughtItem {
    return this.convertBoughtItemRawValueToBoughtItem(form.getRawValue() as BoughtItemFormRawValue | NewBoughtItemFormRawValue);
  }

  resetForm(form: BoughtItemFormGroup, boughtItem: BoughtItemFormGroupInput): void {
    const boughtItemRawValue = this.convertBoughtItemToBoughtItemRawValue({ ...this.getFormDefaults(), ...boughtItem });
    form.reset(
      {
        ...boughtItemRawValue,
        id: { value: boughtItemRawValue.id, disabled: true },
      } as any /* cast to workaround https://github.com/angular/angular/issues/46458 */,
    );
  }

  private getFormDefaults(): BoughtItemFormDefaults {
    const currentTime = dayjs();

    return {
      id: null,
      boughtTime: currentTime,
      billingTime: currentTime,
      paidTime: currentTime,
    };
  }

  private convertBoughtItemRawValueToBoughtItem(
    rawBoughtItem: BoughtItemFormRawValue | NewBoughtItemFormRawValue,
  ): IBoughtItem | NewBoughtItem {
    return {
      ...rawBoughtItem,
      boughtTime: dayjs(rawBoughtItem.boughtTime, DATE_TIME_FORMAT),
      billingTime: dayjs(rawBoughtItem.billingTime, DATE_TIME_FORMAT),
      paidTime: dayjs(rawBoughtItem.paidTime, DATE_TIME_FORMAT),
    };
  }

  private convertBoughtItemToBoughtItemRawValue(
    boughtItem: IBoughtItem | (Partial<NewBoughtItem> & BoughtItemFormDefaults),
  ): BoughtItemFormRawValue | PartialWithRequiredKeyOf<NewBoughtItemFormRawValue> {
    return {
      ...boughtItem,
      boughtTime: boughtItem.boughtTime ? boughtItem.boughtTime.format(DATE_TIME_FORMAT) : undefined,
      billingTime: boughtItem.billingTime ? boughtItem.billingTime.format(DATE_TIME_FORMAT) : undefined,
      paidTime: boughtItem.paidTime ? boughtItem.paidTime.format(DATE_TIME_FORMAT) : undefined,
    };
  }
}
