import { Component, inject, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { finalize, map } from 'rxjs/operators';

import SharedModule from 'app/shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IOfferedItem } from 'app/entities/offered-item/offered-item.model';
import { OfferedItemService } from 'app/entities/offered-item/service/offered-item.service';
import { IUser } from 'app/entities/user/user.model';
import { UserService } from 'app/entities/user/service/user.service';
import { BoughtItemService } from '../service/bought-item.service';
import { IBoughtItem } from '../bought-item.model';
import { BoughtItemFormService, BoughtItemFormGroup } from './bought-item-form.service';

@Component({
  standalone: true,
  selector: 'jhi-bought-item-update',
  templateUrl: './bought-item-update.component.html',
  imports: [SharedModule, FormsModule, ReactiveFormsModule],
})
export class BoughtItemUpdateComponent implements OnInit {
  isSaving = false;
  boughtItem: IBoughtItem | null = null;

  offeredItemsSharedCollection: IOfferedItem[] = [];
  usersSharedCollection: IUser[] = [];

  protected boughtItemService = inject(BoughtItemService);
  protected boughtItemFormService = inject(BoughtItemFormService);
  protected offeredItemService = inject(OfferedItemService);
  protected userService = inject(UserService);
  protected activatedRoute = inject(ActivatedRoute);

  // eslint-disable-next-line @typescript-eslint/member-ordering
  editForm: BoughtItemFormGroup = this.boughtItemFormService.createBoughtItemFormGroup();

  compareOfferedItem = (o1: IOfferedItem | null, o2: IOfferedItem | null): boolean => this.offeredItemService.compareOfferedItem(o1, o2);

  compareUser = (o1: IUser | null, o2: IUser | null): boolean => this.userService.compareUser(o1, o2);

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ boughtItem }) => {
      this.boughtItem = boughtItem;
      if (boughtItem) {
        this.updateForm(boughtItem);
      }

      this.loadRelationshipsOptions();
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const boughtItem = this.boughtItemFormService.getBoughtItem(this.editForm);
    if (boughtItem.id !== null) {
      this.subscribeToSaveResponse(this.boughtItemService.update(boughtItem));
    } else {
      this.subscribeToSaveResponse(this.boughtItemService.create(boughtItem));
    }
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IBoughtItem>>): void {
    result.pipe(finalize(() => this.onSaveFinalize())).subscribe({
      next: () => this.onSaveSuccess(),
      error: () => this.onSaveError(),
    });
  }

  protected onSaveSuccess(): void {
    this.previousState();
  }

  protected onSaveError(): void {
    // Api for inheritance.
  }

  protected onSaveFinalize(): void {
    this.isSaving = false;
  }

  protected updateForm(boughtItem: IBoughtItem): void {
    this.boughtItem = boughtItem;
    this.boughtItemFormService.resetForm(this.editForm, boughtItem);

    this.offeredItemsSharedCollection = this.offeredItemService.addOfferedItemToCollectionIfMissing<IOfferedItem>(
      this.offeredItemsSharedCollection,
      boughtItem.offer,
    );
    this.usersSharedCollection = this.userService.addUserToCollectionIfMissing<IUser>(this.usersSharedCollection, boughtItem.buyer);
  }

  protected loadRelationshipsOptions(): void {
    this.offeredItemService
      .query()
      .pipe(map((res: HttpResponse<IOfferedItem[]>) => res.body ?? []))
      .pipe(
        map((offeredItems: IOfferedItem[]) =>
          this.offeredItemService.addOfferedItemToCollectionIfMissing<IOfferedItem>(offeredItems, this.boughtItem?.offer),
        ),
      )
      .subscribe((offeredItems: IOfferedItem[]) => (this.offeredItemsSharedCollection = offeredItems));

    this.userService
      .query()
      .pipe(map((res: HttpResponse<IUser[]>) => res.body ?? []))
      .pipe(map((users: IUser[]) => this.userService.addUserToCollectionIfMissing<IUser>(users, this.boughtItem?.buyer)))
      .subscribe((users: IUser[]) => (this.usersSharedCollection = users));
  }
}
