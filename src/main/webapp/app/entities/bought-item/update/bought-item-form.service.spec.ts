import { TestBed } from '@angular/core/testing';

import { sampleWithRequiredData, sampleWithNewData } from '../bought-item.test-samples';

import { BoughtItemFormService } from './bought-item-form.service';

describe('BoughtItem Form Service', () => {
  let service: BoughtItemFormService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(BoughtItemFormService);
  });

  describe('Service methods', () => {
    describe('createBoughtItemFormGroup', () => {
      it('should create a new form with FormControl', () => {
        const formGroup = service.createBoughtItemFormGroup();

        expect(formGroup.controls).toEqual(
          expect.objectContaining({
            id: expect.any(Object),
            name: expect.any(Object),
            price: expect.any(Object),
            boughtTime: expect.any(Object),
            billingTime: expect.any(Object),
            paidTime: expect.any(Object),
            amount: expect.any(Object),
            offer: expect.any(Object),
            buyer: expect.any(Object),
          }),
        );
      });

      it('passing IBoughtItem should create a new form with FormGroup', () => {
        const formGroup = service.createBoughtItemFormGroup(sampleWithRequiredData);

        expect(formGroup.controls).toEqual(
          expect.objectContaining({
            id: expect.any(Object),
            name: expect.any(Object),
            price: expect.any(Object),
            boughtTime: expect.any(Object),
            billingTime: expect.any(Object),
            paidTime: expect.any(Object),
            amount: expect.any(Object),
            offer: expect.any(Object),
            buyer: expect.any(Object),
          }),
        );
      });
    });

    describe('getBoughtItem', () => {
      it('should return NewBoughtItem for default BoughtItem initial value', () => {
        const formGroup = service.createBoughtItemFormGroup(sampleWithNewData);

        const boughtItem = service.getBoughtItem(formGroup) as any;

        expect(boughtItem).toMatchObject(sampleWithNewData);
      });

      it('should return NewBoughtItem for empty BoughtItem initial value', () => {
        const formGroup = service.createBoughtItemFormGroup();

        const boughtItem = service.getBoughtItem(formGroup) as any;

        expect(boughtItem).toMatchObject({});
      });

      it('should return IBoughtItem', () => {
        const formGroup = service.createBoughtItemFormGroup(sampleWithRequiredData);

        const boughtItem = service.getBoughtItem(formGroup) as any;

        expect(boughtItem).toMatchObject(sampleWithRequiredData);
      });
    });

    describe('resetForm', () => {
      it('passing IBoughtItem should not enable id FormControl', () => {
        const formGroup = service.createBoughtItemFormGroup();
        expect(formGroup.controls.id.disabled).toBe(true);

        service.resetForm(formGroup, sampleWithRequiredData);

        expect(formGroup.controls.id.disabled).toBe(true);
      });

      it('passing NewBoughtItem should disable id FormControl', () => {
        const formGroup = service.createBoughtItemFormGroup(sampleWithRequiredData);
        expect(formGroup.controls.id.disabled).toBe(true);

        service.resetForm(formGroup, { id: null });

        expect(formGroup.controls.id.disabled).toBe(true);
      });
    });
  });
});
