import { ComponentFixture, TestBed } from '@angular/core/testing';
import { provideHttpClient, HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { of, Subject, from } from 'rxjs';

import { IOfferedItem } from 'app/entities/offered-item/offered-item.model';
import { OfferedItemService } from 'app/entities/offered-item/service/offered-item.service';
import { IUser } from 'app/entities/user/user.model';
import { UserService } from 'app/entities/user/service/user.service';
import { IBoughtItem } from '../bought-item.model';
import { BoughtItemService } from '../service/bought-item.service';
import { BoughtItemFormService } from './bought-item-form.service';

import { BoughtItemUpdateComponent } from './bought-item-update.component';

describe('BoughtItem Management Update Component', () => {
  let comp: BoughtItemUpdateComponent;
  let fixture: ComponentFixture<BoughtItemUpdateComponent>;
  let activatedRoute: ActivatedRoute;
  let boughtItemFormService: BoughtItemFormService;
  let boughtItemService: BoughtItemService;
  let offeredItemService: OfferedItemService;
  let userService: UserService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [BoughtItemUpdateComponent],
      providers: [
        provideHttpClient(),
        FormBuilder,
        {
          provide: ActivatedRoute,
          useValue: {
            params: from([{}]),
          },
        },
      ],
    })
      .overrideTemplate(BoughtItemUpdateComponent, '')
      .compileComponents();

    fixture = TestBed.createComponent(BoughtItemUpdateComponent);
    activatedRoute = TestBed.inject(ActivatedRoute);
    boughtItemFormService = TestBed.inject(BoughtItemFormService);
    boughtItemService = TestBed.inject(BoughtItemService);
    offeredItemService = TestBed.inject(OfferedItemService);
    userService = TestBed.inject(UserService);

    comp = fixture.componentInstance;
  });

  describe('ngOnInit', () => {
    it('Should call OfferedItem query and add missing value', () => {
      const boughtItem: IBoughtItem = { id: 456 };
      const offer: IOfferedItem = { id: 7442 };
      boughtItem.offer = offer;

      const offeredItemCollection: IOfferedItem[] = [{ id: 20000 }];
      jest.spyOn(offeredItemService, 'query').mockReturnValue(of(new HttpResponse({ body: offeredItemCollection })));
      const additionalOfferedItems = [offer];
      const expectedCollection: IOfferedItem[] = [...additionalOfferedItems, ...offeredItemCollection];
      jest.spyOn(offeredItemService, 'addOfferedItemToCollectionIfMissing').mockReturnValue(expectedCollection);

      activatedRoute.data = of({ boughtItem });
      comp.ngOnInit();

      expect(offeredItemService.query).toHaveBeenCalled();
      expect(offeredItemService.addOfferedItemToCollectionIfMissing).toHaveBeenCalledWith(
        offeredItemCollection,
        ...additionalOfferedItems.map(expect.objectContaining),
      );
      expect(comp.offeredItemsSharedCollection).toEqual(expectedCollection);
    });

    it('Should call User query and add missing value', () => {
      const boughtItem: IBoughtItem = { id: 456 };
      const buyer: IUser = { id: 11997 };
      boughtItem.buyer = buyer;

      const userCollection: IUser[] = [{ id: 29909 }];
      jest.spyOn(userService, 'query').mockReturnValue(of(new HttpResponse({ body: userCollection })));
      const additionalUsers = [buyer];
      const expectedCollection: IUser[] = [...additionalUsers, ...userCollection];
      jest.spyOn(userService, 'addUserToCollectionIfMissing').mockReturnValue(expectedCollection);

      activatedRoute.data = of({ boughtItem });
      comp.ngOnInit();

      expect(userService.query).toHaveBeenCalled();
      expect(userService.addUserToCollectionIfMissing).toHaveBeenCalledWith(
        userCollection,
        ...additionalUsers.map(expect.objectContaining),
      );
      expect(comp.usersSharedCollection).toEqual(expectedCollection);
    });

    it('Should update editForm', () => {
      const boughtItem: IBoughtItem = { id: 456 };
      const offer: IOfferedItem = { id: 24363 };
      boughtItem.offer = offer;
      const buyer: IUser = { id: 5263 };
      boughtItem.buyer = buyer;

      activatedRoute.data = of({ boughtItem });
      comp.ngOnInit();

      expect(comp.offeredItemsSharedCollection).toContain(offer);
      expect(comp.usersSharedCollection).toContain(buyer);
      expect(comp.boughtItem).toEqual(boughtItem);
    });
  });

  describe('save', () => {
    it('Should call update service on save for existing entity', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<IBoughtItem>>();
      const boughtItem = { id: 123 };
      jest.spyOn(boughtItemFormService, 'getBoughtItem').mockReturnValue(boughtItem);
      jest.spyOn(boughtItemService, 'update').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ boughtItem });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.next(new HttpResponse({ body: boughtItem }));
      saveSubject.complete();

      // THEN
      expect(boughtItemFormService.getBoughtItem).toHaveBeenCalled();
      expect(comp.previousState).toHaveBeenCalled();
      expect(boughtItemService.update).toHaveBeenCalledWith(expect.objectContaining(boughtItem));
      expect(comp.isSaving).toEqual(false);
    });

    it('Should call create service on save for new entity', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<IBoughtItem>>();
      const boughtItem = { id: 123 };
      jest.spyOn(boughtItemFormService, 'getBoughtItem').mockReturnValue({ id: null });
      jest.spyOn(boughtItemService, 'create').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ boughtItem: null });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.next(new HttpResponse({ body: boughtItem }));
      saveSubject.complete();

      // THEN
      expect(boughtItemFormService.getBoughtItem).toHaveBeenCalled();
      expect(boughtItemService.create).toHaveBeenCalled();
      expect(comp.isSaving).toEqual(false);
      expect(comp.previousState).toHaveBeenCalled();
    });

    it('Should set isSaving to false on error', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<IBoughtItem>>();
      const boughtItem = { id: 123 };
      jest.spyOn(boughtItemService, 'update').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ boughtItem });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.error('This is an error!');

      // THEN
      expect(boughtItemService.update).toHaveBeenCalled();
      expect(comp.isSaving).toEqual(false);
      expect(comp.previousState).not.toHaveBeenCalled();
    });
  });

  describe('Compare relationships', () => {
    describe('compareOfferedItem', () => {
      it('Should forward to offeredItemService', () => {
        const entity = { id: 123 };
        const entity2 = { id: 456 };
        jest.spyOn(offeredItemService, 'compareOfferedItem');
        comp.compareOfferedItem(entity, entity2);
        expect(offeredItemService.compareOfferedItem).toHaveBeenCalledWith(entity, entity2);
      });
    });

    describe('compareUser', () => {
      it('Should forward to userService', () => {
        const entity = { id: 123 };
        const entity2 = { id: 456 };
        jest.spyOn(userService, 'compareUser');
        comp.compareUser(entity, entity2);
        expect(userService.compareUser).toHaveBeenCalledWith(entity, entity2);
      });
    });
  });
});
