import { TestBed } from '@angular/core/testing';
import { provideHttpClientTesting, HttpTestingController } from '@angular/common/http/testing';
import { provideHttpClient } from '@angular/common/http';

import { IBoughtItem } from '../bought-item.model';
import { sampleWithRequiredData, sampleWithNewData, sampleWithPartialData, sampleWithFullData } from '../bought-item.test-samples';

import { BoughtItemService, RestBoughtItem } from './bought-item.service';

const requireRestSample: RestBoughtItem = {
  ...sampleWithRequiredData,
  boughtTime: sampleWithRequiredData.boughtTime?.toJSON(),
  billingTime: sampleWithRequiredData.billingTime?.toJSON(),
  paidTime: sampleWithRequiredData.paidTime?.toJSON(),
};

describe('BoughtItem Service', () => {
  let service: BoughtItemService;
  let httpMock: HttpTestingController;
  let expectedResult: IBoughtItem | IBoughtItem[] | boolean | null;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [provideHttpClient(), provideHttpClientTesting()],
    });
    expectedResult = null;
    service = TestBed.inject(BoughtItemService);
    httpMock = TestBed.inject(HttpTestingController);
  });

  describe('Service methods', () => {
    it('should find an element', () => {
      const returnedFromService = { ...requireRestSample };
      const expected = { ...sampleWithRequiredData };

      service.find(123).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'GET' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should create a BoughtItem', () => {
      const boughtItem = { ...sampleWithNewData };
      const returnedFromService = { ...requireRestSample };
      const expected = { ...sampleWithRequiredData };

      service.create(boughtItem).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'POST' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should update a BoughtItem', () => {
      const boughtItem = { ...sampleWithRequiredData };
      const returnedFromService = { ...requireRestSample };
      const expected = { ...sampleWithRequiredData };

      service.update(boughtItem).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'PUT' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should partial update a BoughtItem', () => {
      const patchObject = { ...sampleWithPartialData };
      const returnedFromService = { ...requireRestSample };
      const expected = { ...sampleWithRequiredData };

      service.partialUpdate(patchObject).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'PATCH' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should return a list of BoughtItem', () => {
      const returnedFromService = { ...requireRestSample };

      const expected = { ...sampleWithRequiredData };

      service.query().subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'GET' });
      req.flush([returnedFromService]);
      httpMock.verify();
      expect(expectedResult).toMatchObject([expected]);
    });

    it('should delete a BoughtItem', () => {
      const expected = true;

      service.delete(123).subscribe(resp => (expectedResult = resp.ok));

      const req = httpMock.expectOne({ method: 'DELETE' });
      req.flush({ status: 200 });
      expect(expectedResult).toBe(expected);
    });

    describe('addBoughtItemToCollectionIfMissing', () => {
      it('should add a BoughtItem to an empty array', () => {
        const boughtItem: IBoughtItem = sampleWithRequiredData;
        expectedResult = service.addBoughtItemToCollectionIfMissing([], boughtItem);
        expect(expectedResult).toHaveLength(1);
        expect(expectedResult).toContain(boughtItem);
      });

      it('should not add a BoughtItem to an array that contains it', () => {
        const boughtItem: IBoughtItem = sampleWithRequiredData;
        const boughtItemCollection: IBoughtItem[] = [
          {
            ...boughtItem,
          },
          sampleWithPartialData,
        ];
        expectedResult = service.addBoughtItemToCollectionIfMissing(boughtItemCollection, boughtItem);
        expect(expectedResult).toHaveLength(2);
      });

      it("should add a BoughtItem to an array that doesn't contain it", () => {
        const boughtItem: IBoughtItem = sampleWithRequiredData;
        const boughtItemCollection: IBoughtItem[] = [sampleWithPartialData];
        expectedResult = service.addBoughtItemToCollectionIfMissing(boughtItemCollection, boughtItem);
        expect(expectedResult).toHaveLength(2);
        expect(expectedResult).toContain(boughtItem);
      });

      it('should add only unique BoughtItem to an array', () => {
        const boughtItemArray: IBoughtItem[] = [sampleWithRequiredData, sampleWithPartialData, sampleWithFullData];
        const boughtItemCollection: IBoughtItem[] = [sampleWithRequiredData];
        expectedResult = service.addBoughtItemToCollectionIfMissing(boughtItemCollection, ...boughtItemArray);
        expect(expectedResult).toHaveLength(3);
      });

      it('should accept varargs', () => {
        const boughtItem: IBoughtItem = sampleWithRequiredData;
        const boughtItem2: IBoughtItem = sampleWithPartialData;
        expectedResult = service.addBoughtItemToCollectionIfMissing([], boughtItem, boughtItem2);
        expect(expectedResult).toHaveLength(2);
        expect(expectedResult).toContain(boughtItem);
        expect(expectedResult).toContain(boughtItem2);
      });

      it('should accept null and undefined values', () => {
        const boughtItem: IBoughtItem = sampleWithRequiredData;
        expectedResult = service.addBoughtItemToCollectionIfMissing([], null, boughtItem, undefined);
        expect(expectedResult).toHaveLength(1);
        expect(expectedResult).toContain(boughtItem);
      });

      it('should return initial array if no BoughtItem is added', () => {
        const boughtItemCollection: IBoughtItem[] = [sampleWithRequiredData];
        expectedResult = service.addBoughtItemToCollectionIfMissing(boughtItemCollection, undefined, null);
        expect(expectedResult).toEqual(boughtItemCollection);
      });
    });

    describe('compareBoughtItem', () => {
      it('Should return true if both entities are null', () => {
        const entity1 = null;
        const entity2 = null;

        const compareResult = service.compareBoughtItem(entity1, entity2);

        expect(compareResult).toEqual(true);
      });

      it('Should return false if one entity is null', () => {
        const entity1 = { id: 123 };
        const entity2 = null;

        const compareResult1 = service.compareBoughtItem(entity1, entity2);
        const compareResult2 = service.compareBoughtItem(entity2, entity1);

        expect(compareResult1).toEqual(false);
        expect(compareResult2).toEqual(false);
      });

      it('Should return false if primaryKey differs', () => {
        const entity1 = { id: 123 };
        const entity2 = { id: 456 };

        const compareResult1 = service.compareBoughtItem(entity1, entity2);
        const compareResult2 = service.compareBoughtItem(entity2, entity1);

        expect(compareResult1).toEqual(false);
        expect(compareResult2).toEqual(false);
      });

      it('Should return false if primaryKey matches', () => {
        const entity1 = { id: 123 };
        const entity2 = { id: 123 };

        const compareResult1 = service.compareBoughtItem(entity1, entity2);
        const compareResult2 = service.compareBoughtItem(entity2, entity1);

        expect(compareResult1).toEqual(true);
        expect(compareResult2).toEqual(true);
      });
    });
  });

  afterEach(() => {
    httpMock.verify();
  });
});
