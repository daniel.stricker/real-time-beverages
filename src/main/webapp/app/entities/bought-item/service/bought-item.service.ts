import { inject, Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { map, Observable } from 'rxjs';

import dayjs from 'dayjs/esm';

import { isPresent } from 'app/core/util/operators';
import { ApplicationConfigService } from 'app/core/config/application-config.service';
import { createRequestOption } from 'app/core/request/request-util';
import { IBoughtItem, NewBoughtItem } from '../bought-item.model';

export type PartialUpdateBoughtItem = Partial<IBoughtItem> & Pick<IBoughtItem, 'id'>;

type RestOf<T extends IBoughtItem | NewBoughtItem> = Omit<T, 'boughtTime' | 'billingTime' | 'paidTime'> & {
  boughtTime?: string | null;
  billingTime?: string | null;
  paidTime?: string | null;
};

export type RestBoughtItem = RestOf<IBoughtItem>;

export type NewRestBoughtItem = RestOf<NewBoughtItem>;

export type PartialUpdateRestBoughtItem = RestOf<PartialUpdateBoughtItem>;

export type EntityResponseType = HttpResponse<IBoughtItem>;
export type EntityArrayResponseType = HttpResponse<IBoughtItem[]>;

@Injectable({ providedIn: 'root' })
export class BoughtItemService {
  protected http = inject(HttpClient);
  protected applicationConfigService = inject(ApplicationConfigService);

  protected resourceUrl = this.applicationConfigService.getEndpointFor('api/bought-items');

  create(boughtItem: NewBoughtItem): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(boughtItem);
    return this.http
      .post<RestBoughtItem>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map(res => this.convertResponseFromServer(res)));
  }

  update(boughtItem: IBoughtItem): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(boughtItem);
    return this.http
      .put<RestBoughtItem>(`${this.resourceUrl}/${this.getBoughtItemIdentifier(boughtItem)}`, copy, { observe: 'response' })
      .pipe(map(res => this.convertResponseFromServer(res)));
  }

  partialUpdate(boughtItem: PartialUpdateBoughtItem): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(boughtItem);
    return this.http
      .patch<RestBoughtItem>(`${this.resourceUrl}/${this.getBoughtItemIdentifier(boughtItem)}`, copy, { observe: 'response' })
      .pipe(map(res => this.convertResponseFromServer(res)));
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http
      .get<RestBoughtItem>(`${this.resourceUrl}/${id}`, { observe: 'response' })
      .pipe(map(res => this.convertResponseFromServer(res)));
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http
      .get<RestBoughtItem[]>(this.resourceUrl, { params: options, observe: 'response' })
      .pipe(map(res => this.convertResponseArrayFromServer(res)));
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  getBoughtItemIdentifier(boughtItem: Pick<IBoughtItem, 'id'>): number {
    return boughtItem.id;
  }

  compareBoughtItem(o1: Pick<IBoughtItem, 'id'> | null, o2: Pick<IBoughtItem, 'id'> | null): boolean {
    return o1 && o2 ? this.getBoughtItemIdentifier(o1) === this.getBoughtItemIdentifier(o2) : o1 === o2;
  }

  addBoughtItemToCollectionIfMissing<Type extends Pick<IBoughtItem, 'id'>>(
    boughtItemCollection: Type[],
    ...boughtItemsToCheck: (Type | null | undefined)[]
  ): Type[] {
    const boughtItems: Type[] = boughtItemsToCheck.filter(isPresent);
    if (boughtItems.length > 0) {
      const boughtItemCollectionIdentifiers = boughtItemCollection.map(boughtItemItem => this.getBoughtItemIdentifier(boughtItemItem));
      const boughtItemsToAdd = boughtItems.filter(boughtItemItem => {
        const boughtItemIdentifier = this.getBoughtItemIdentifier(boughtItemItem);
        if (boughtItemCollectionIdentifiers.includes(boughtItemIdentifier)) {
          return false;
        }
        boughtItemCollectionIdentifiers.push(boughtItemIdentifier);
        return true;
      });
      return [...boughtItemsToAdd, ...boughtItemCollection];
    }
    return boughtItemCollection;
  }

  protected convertDateFromClient<T extends IBoughtItem | NewBoughtItem | PartialUpdateBoughtItem>(boughtItem: T): RestOf<T> {
    return {
      ...boughtItem,
      boughtTime: boughtItem.boughtTime?.toJSON() ?? null,
      billingTime: boughtItem.billingTime?.toJSON() ?? null,
      paidTime: boughtItem.paidTime?.toJSON() ?? null,
    };
  }

  protected convertDateFromServer(restBoughtItem: RestBoughtItem): IBoughtItem {
    return {
      ...restBoughtItem,
      boughtTime: restBoughtItem.boughtTime ? dayjs(restBoughtItem.boughtTime) : undefined,
      billingTime: restBoughtItem.billingTime ? dayjs(restBoughtItem.billingTime) : undefined,
      paidTime: restBoughtItem.paidTime ? dayjs(restBoughtItem.paidTime) : undefined,
    };
  }

  protected convertResponseFromServer(res: HttpResponse<RestBoughtItem>): HttpResponse<IBoughtItem> {
    return res.clone({
      body: res.body ? this.convertDateFromServer(res.body) : null,
    });
  }

  protected convertResponseArrayFromServer(res: HttpResponse<RestBoughtItem[]>): HttpResponse<IBoughtItem[]> {
    return res.clone({
      body: res.body ? res.body.map(item => this.convertDateFromServer(item)) : null,
    });
  }
}
