import { Routes } from '@angular/router';

import { UserRouteAccessService } from 'app/core/auth/user-route-access.service';
import { ASC } from 'app/config/navigation.constants';
import { BoughtItemComponent } from './list/bought-item.component';
import { BoughtItemDetailComponent } from './detail/bought-item-detail.component';
import { BoughtItemUpdateComponent } from './update/bought-item-update.component';
import BoughtItemResolve from './route/bought-item-routing-resolve.service';

const boughtItemRoute: Routes = [
  {
    path: '',
    component: BoughtItemComponent,
    data: {
      defaultSort: 'id,' + ASC,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/view',
    component: BoughtItemDetailComponent,
    resolve: {
      boughtItem: BoughtItemResolve,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: 'new',
    component: BoughtItemUpdateComponent,
    resolve: {
      boughtItem: BoughtItemResolve,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/edit',
    component: BoughtItemUpdateComponent,
    resolve: {
      boughtItem: BoughtItemResolve,
    },
    canActivate: [UserRouteAccessService],
  },
];

export default boughtItemRoute;
