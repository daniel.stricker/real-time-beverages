import { inject } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { ActivatedRouteSnapshot, Router } from '@angular/router';
import { of, EMPTY, Observable } from 'rxjs';
import { mergeMap } from 'rxjs/operators';

import { IBoughtItem } from '../bought-item.model';
import { BoughtItemService } from '../service/bought-item.service';

const boughtItemResolve = (route: ActivatedRouteSnapshot): Observable<null | IBoughtItem> => {
  const id = route.params['id'];
  if (id) {
    return inject(BoughtItemService)
      .find(id)
      .pipe(
        mergeMap((boughtItem: HttpResponse<IBoughtItem>) => {
          if (boughtItem.body) {
            return of(boughtItem.body);
          } else {
            inject(Router).navigate(['404']);
            return EMPTY;
          }
        }),
      );
  }
  return of(null);
};

export default boughtItemResolve;
