import { Component, inject } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

import SharedModule from 'app/shared/shared.module';
import { ITEM_DELETED_EVENT } from 'app/config/navigation.constants';
import { IBoughtItem } from '../bought-item.model';
import { BoughtItemService } from '../service/bought-item.service';

@Component({
  standalone: true,
  templateUrl: './bought-item-delete-dialog.component.html',
  imports: [SharedModule, FormsModule],
})
export class BoughtItemDeleteDialogComponent {
  boughtItem?: IBoughtItem;

  protected boughtItemService = inject(BoughtItemService);
  protected activeModal = inject(NgbActiveModal);

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.boughtItemService.delete(id).subscribe(() => {
      this.activeModal.close(ITEM_DELETED_EVENT);
    });
  }
}
