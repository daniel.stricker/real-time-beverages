import dayjs from 'dayjs/esm';
import { IOfferedItem } from 'app/entities/offered-item/offered-item.model';
import { IUser } from 'app/entities/user/user.model';

export interface IBoughtItem {
  id: number;
  name?: string | null;
  price?: number | null;
  boughtTime?: dayjs.Dayjs | null;
  billingTime?: dayjs.Dayjs | null;
  paidTime?: dayjs.Dayjs | null;
  amount?: number | null;
  offer?: IOfferedItem | null;
  buyer?: Pick<IUser, 'id'> | null;
}

export type NewBoughtItem = Omit<IBoughtItem, 'id'> & { id: null };
