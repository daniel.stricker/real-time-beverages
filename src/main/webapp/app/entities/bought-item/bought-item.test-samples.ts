import dayjs from 'dayjs/esm';

import { IBoughtItem, NewBoughtItem } from './bought-item.model';

export const sampleWithRequiredData: IBoughtItem = {
  id: 14840,
  name: 'hiedurch expandieren tremolieren',
  price: 28511.48,
  boughtTime: dayjs('2024-05-07T18:20'),
  amount: 26311,
};

export const sampleWithPartialData: IBoughtItem = {
  id: 20519,
  name: 'südwestlich Knöchel',
  price: 25146.99,
  boughtTime: dayjs('2024-05-07T18:11'),
  amount: 32076,
};

export const sampleWithFullData: IBoughtItem = {
  id: 8865,
  name: 'since Proton',
  price: 17775.01,
  boughtTime: dayjs('2024-05-08T08:16'),
  billingTime: dayjs('2024-05-08T06:35'),
  paidTime: dayjs('2024-05-08T01:20'),
  amount: 13750,
};

export const sampleWithNewData: NewBoughtItem = {
  name: 'unerachtet praktisch Sport',
  price: 3919.22,
  boughtTime: dayjs('2024-05-08T12:03'),
  amount: 9648,
  id: null,
};

Object.freeze(sampleWithNewData);
Object.freeze(sampleWithRequiredData);
Object.freeze(sampleWithPartialData);
Object.freeze(sampleWithFullData);
