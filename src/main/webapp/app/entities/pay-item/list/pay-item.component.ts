import { ChangeDetectionStrategy, Component, computed, inject, NgZone, OnInit, signal } from '@angular/core';
import { ActivatedRoute, Data, ParamMap, Router, RouterModule } from '@angular/router';
import { combineLatest, filter, Observable, Subscription, tap } from 'rxjs';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import SharedModule from 'app/shared/shared.module';
import { SortByDirective, SortDirective, SortService, type SortState, sortStateSignal } from 'app/shared/sort';
import { DurationPipe, FormatMediumDatePipe, FormatMediumDatetimePipe } from 'app/shared/date';
import { FormControl, FormGroup, FormsModule, NonNullableFormBuilder, ReactiveFormsModule } from '@angular/forms';
import { DEFAULT_SORT_DATA, ITEM_DELETED_EVENT, SORT } from 'app/config/navigation.constants';
import { LANGUAGES } from '../../../config/language.constants';
import { toSignal } from '@angular/core/rxjs-interop';
import { MatFormField, MatHint, MatLabel, MatOption, MatSelect } from '@angular/material/select';
import { MatDatepickerModule, MatDatepickerToggle, MatDateRangeInput, MatDateRangePicker } from '@angular/material/datepicker';
import { MatNativeDateModule } from '@angular/material/core';
import { MatFormFieldModule } from '@angular/material/form-field';
import { IBoughtItem } from '../../bought-item/bought-item.model';
import { BoughtItemDeleteDialogComponent } from '../../bought-item/delete/bought-item-delete-dialog.component';
import { BoughtItemService } from '../../bought-item/service/bought-item.service';
import { EntityArrayResponseType } from '../../user/service/user.service';
import { PayItemService } from '../service/pay-item.service';

@Component({
  standalone: true,
  selector: 'jhi-bought-item',
  templateUrl: './pay-item.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
  imports: [
    RouterModule,
    FormsModule,
    SharedModule,
    SortDirective,
    SortByDirective,
    DurationPipe,
    FormatMediumDatetimePipe,
    FormatMediumDatePipe,
    ReactiveFormsModule,
    MatSelect,
    MatFormField,
    FormsModule,
    MatFormFieldModule,
    MatDatepickerModule,
    ReactiveFormsModule,
    MatOption,
    MatLabel,
    MatDateRangeInput,
    MatDatepickerToggle,
    MatNativeDateModule,
    MatDateRangePicker,
    MatHint,
  ],
})
export class PayItemComponent implements OnInit {
  public router = inject(Router);
  subscription: Subscription | null = null;
  unfilteredBoughtItems = signal<IBoughtItem[] | undefined>(undefined);
  isLoading = false;
  sortState = sortStateSignal({ order: 'asc', predicate: 'id' });
  boughtItemService = inject(BoughtItemService);
  payItemService = inject(PayItemService);
  formbuilder = inject(NonNullableFormBuilder);
  filterFormGroup = new FormGroup({
    userID: new FormControl<number | undefined>(undefined),
    status: new FormControl<string | null>(null),
  });
  filterValueChanges = toSignal(this.filterFormGroup.valueChanges);
  filterStatusChanges = toSignal(this.controls.status.valueChanges);
  activatedRoute = inject(ActivatedRoute);
  boughtItems = computed(() => {
    const filter = this.filterValueChanges();
    const unfilteredBoughtItems = this.unfilteredBoughtItems();
    const filterStatusChanges = this.filterStatusChanges();
    if (filter && unfilteredBoughtItems) {
      let filteredBoughtItems = unfilteredBoughtItems;
      if (filter.userID) filteredBoughtItems = filteredBoughtItems.filter(boughtItem => filter.userID! === boughtItem.buyer?.id!);
      if (filterStatusChanges) {
        switch (filterStatusChanges) {
          case 'Noch abzurechnen':
            filteredBoughtItems = filteredBoughtItems.filter(boughtItem => !boughtItem.billingTime);
            break;
          case 'Noch zu zahlen':
            filteredBoughtItems = filteredBoughtItems.filter(boughtItem => !boughtItem.paidTime && boughtItem.billingTime);
            break;
          case 'Bezahlt':
            filteredBoughtItems = filteredBoughtItems.filter(boughtItem => boughtItem.paidTime);
            break;
        }
      }
      return filteredBoughtItems;
    } else {
      return undefined;
    }
  });
  protected sortService = inject(SortService);
  protected modalService = inject(NgbModal);
  protected ngZone = inject(NgZone);
  protected readonly languages = LANGUAGES;

  get controls() {
    return this.filterFormGroup.controls;
  }

  trackId = (_index: number, item: IBoughtItem): number => this.boughtItemService.getBoughtItemIdentifier(item);

  ngOnInit(): void {
    this.subscription = combineLatest([this.activatedRoute.queryParamMap, this.activatedRoute.data])
      .pipe(
        tap(([params, data]) => this.fillComponentAttributeFromRoute(params, data)),
        tap(() => {
          if (this.boughtItems() == null || this.boughtItems()!.length === 0) {
            this.load();
          }
        }),
      )
      .subscribe();
  }

  delete(boughtItem: IBoughtItem): void {
    const modalRef = this.modalService.open(BoughtItemDeleteDialogComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.boughtItem = boughtItem;
    // unsubscribe not needed because closed completes on modal close
    modalRef.closed
      .pipe(
        filter(reason => reason === ITEM_DELETED_EVENT),
        tap(() => this.load()),
      )
      .subscribe();
  }

  load(): void {
    this.queryBackend().subscribe({
      next: (res: EntityArrayResponseType) => {
        this.onResponseSuccess(res);
      },
    });
  }

  makeInvoice() {
    this.payItemService.createBilling(this.controls.userID.value!, new Date().toString()).subscribe(response => {
      if (response.status === 200) this.load();
    });
  }

  getUsers() {
    const uniqueUsersMap = new Map<number, any>();
    const users = this.unfilteredBoughtItems()?.map(boughtItem => boughtItem.buyer);
    users?.forEach(user => {
      if (user?.id) uniqueUsersMap.set(user.id, user);
    });
    return Array.from(uniqueUsersMap.values());
  }

  navigateToWithComponentValues(event: SortState): void {
    this.handleNavigation(event);
  }

  protected fillComponentAttributeFromRoute(params: ParamMap, data: Data): void {
    this.sortState.set(this.sortService.parseSortParam(params.get(SORT) ?? data[DEFAULT_SORT_DATA]));
  }

  protected onResponseSuccess(response: EntityArrayResponseType): void {
    const dataFromBody = this.fillComponentAttributesFromResponseBody(response.body);
    this.unfilteredBoughtItems.set(this.refineData(dataFromBody));
  }

  protected refineData(data: IBoughtItem[]): IBoughtItem[] {
    const { predicate, order } = this.sortState();
    return predicate && order ? data.sort(this.sortService.startSort({ predicate, order })) : data;
  }

  protected fillComponentAttributesFromResponseBody(data: IBoughtItem[] | null): IBoughtItem[] {
    return data ?? [];
  }

  protected queryBackend(): Observable<EntityArrayResponseType> {
    this.isLoading = true;
    const queryObject: any = {
      sort: this.sortService.buildSortParam(this.sortState()),
    };
    return this.boughtItemService.query(queryObject).pipe(tap(() => (this.isLoading = false)));
  }

  protected handleNavigation(sortState: SortState): void {
    const queryParamsObj = {
      sort: this.sortService.buildSortParam(sortState),
    };

    this.ngZone.run(() => {
      this.router.navigate(['./'], {
        relativeTo: this.activatedRoute,
        queryParams: queryParamsObj,
      });
    });
  }
}
