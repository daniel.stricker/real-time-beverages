import { Routes } from '@angular/router';

import { UserRouteAccessService } from 'app/core/auth/user-route-access.service';
import { ASC } from 'app/config/navigation.constants';
import { PayItemComponent } from './list/pay-item.component';

const payItemRoutes: Routes = [
  {
    path: '',
    component: PayItemComponent,
    data: {
      defaultSort: 'id,' + ASC,
    },
    canActivate: [UserRouteAccessService],
  },
];

export default payItemRoutes;
