import { inject, Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import dayjs from 'dayjs/esm';
import { ApplicationConfigService } from 'app/core/config/application-config.service';
import { IBoughtItem } from '../../bought-item/bought-item.model';

@Injectable({ providedIn: 'root' })
export class PayItemService {
  protected http = inject(HttpClient);
  protected applicationConfigService = inject(ApplicationConfigService);

  protected resourceUrl = this.applicationConfigService.getEndpointFor('api/store');

  payUser(id: number, date: string) {
    return this.http.post(this.resourceUrl + '/pay', { userId: id, billingTime: dayjs(date) }, { observe: 'response' });
  }

  payItems(items: IBoughtItem[]) {
    return this.http.post(this.resourceUrl + '/pay-items', { items }, { observe: 'response' });
  }

  createBilling(id: number, time: string) {
    return this.http.post(
      this.resourceUrl + '/billing',
      {
        userId: id,
        beforeIncluding: dayjs(time),
      },
      { observe: 'response' },
    );
  }

  getUserInvoices(id: number) {
    return this.http.get(this.resourceUrl + `/user-invoices/${id}}`, { observe: 'response' });
  }
}
