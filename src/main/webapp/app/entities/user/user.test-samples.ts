import { IUser } from './user.model';

export const sampleWithRequiredData: IUser = {
  id: 28303,
  login: 'aFR',
};

export const sampleWithPartialData: IUser = {
  id: 3629,
  login: 'NVu',
};

export const sampleWithFullData: IUser = {
  id: 14933,
  login: '=oXt@Q\\35DtD\\GrIqL\\%dms\\~C3',
};
Object.freeze(sampleWithRequiredData);
Object.freeze(sampleWithPartialData);
Object.freeze(sampleWithFullData);
