import { IAuthority, NewAuthority } from './authority.model';

export const sampleWithRequiredData: IAuthority = {
  name: 'b2216059-89c4-4443-b0f1-81ae10ae3c30',
};

export const sampleWithPartialData: IAuthority = {
  name: 'aefe009f-5ac1-4922-b8ca-d5725cf6c05f',
};

export const sampleWithFullData: IAuthority = {
  name: '6ae447ff-9371-4d8c-9e46-f5527e681091',
};

export const sampleWithNewData: NewAuthority = {
  name: null,
};

Object.freeze(sampleWithNewData);
Object.freeze(sampleWithRequiredData);
Object.freeze(sampleWithPartialData);
Object.freeze(sampleWithFullData);
