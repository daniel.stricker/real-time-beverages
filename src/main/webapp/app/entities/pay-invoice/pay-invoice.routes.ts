import { Routes } from '@angular/router';

import { UserRouteAccessService } from 'app/core/auth/user-route-access.service';
import { ASC } from 'app/config/navigation.constants';
import { PayInvoiceComponent } from './list/pay-invoice.component';

const payInvoiceRoutes: Routes = [
  {
    path: '',
    component: PayInvoiceComponent,
    data: {
      defaultSort: 'id,' + ASC,
    },
    canActivate: [UserRouteAccessService],
  },
];

export default payInvoiceRoutes;
