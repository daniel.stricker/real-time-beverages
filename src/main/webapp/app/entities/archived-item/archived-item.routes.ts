import { Routes } from '@angular/router';

import { UserRouteAccessService } from 'app/core/auth/user-route-access.service';
import { ASC } from 'app/config/navigation.constants';
import { ArchivedItemComponent } from './list/archived-item.component';

const offeredItemRoute: Routes = [
  {
    path: '',
    component: ArchivedItemComponent,
    data: {
      defaultSort: 'id,' + ASC,
    },
    canActivate: [UserRouteAccessService],
  },
];

export default offeredItemRoute;
