export interface IOfferedItem {
  id: number;
  name?: string | null;
  stock?: number | null;
  price?: number | null;
  hidden?: boolean | null;
  image?: string | null;
  imageContentType?: string | null;
}

export type NewOfferedItem = Omit<IOfferedItem, 'id'> & { id: null };
