import { IOfferedItem, NewOfferedItem } from './archived-item.model';

export const sampleWithRequiredData: IOfferedItem = {
  id: 13091,
  name: 'Computer',
  stock: 12199,
  price: 20575.97,
  hidden: false,
};

export const sampleWithPartialData: IOfferedItem = {
  id: 24071,
  name: 'eek jubeln Kind',
  stock: 31809,
  price: 1412.83,
  hidden: true,
};

export const sampleWithFullData: IOfferedItem = {
  id: 10369,
  name: 'schweißen uh-huh',
  stock: 5251,
  price: 23194.87,
  hidden: true,
  image: '../fake-data/blob/hipster.png',
  imageContentType: 'unknown',
};

export const sampleWithNewData: NewOfferedItem = {
  name: 'bewunderungswürdig unbeschadet',
  stock: 12129,
  price: 20398.15,
  hidden: true,
  id: null,
};

Object.freeze(sampleWithNewData);
Object.freeze(sampleWithRequiredData);
Object.freeze(sampleWithPartialData);
Object.freeze(sampleWithFullData);
