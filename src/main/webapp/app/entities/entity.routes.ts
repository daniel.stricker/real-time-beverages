import { Routes } from '@angular/router';

const routes: Routes = [
  {
    path: 'authority',
    data: { pageTitle: 'realTimeBeveragesApp.adminAuthority.home.title' },
    loadChildren: () => import('./admin/authority/authority.routes'),
  },
  {
    path: 'order',
    data: { pageTitle: 'Bestellung' },
    loadChildren: () => import('./order/order.routes'),
  },
  {
    path: 'offered-item',
    data: { pageTitle: 'realTimeBeveragesApp.offeredItem.home.title' },
    loadChildren: () => import('./offered-item/offered-item.routes'),
  },
  {
    path: 'bought-item',
    data: { pageTitle: 'realTimeBeveragesApp.boughtItem.home.title' },
    loadChildren: () => import('./bought-item/bought-item.routes'),
  },
  {
    path: 'archived-item',
    data: {
      pageTitle: 'realTimeBeveragesApp.boughtItem.home.title',
    },
    loadChildren: () => import('./archived-item/archived-item.routes'),
  },
  {
    path: 'pay-items',
    data: { pageTitle: 'realTimeBeveragesApp.adminAuthority.home.title' },
    loadChildren: () => import('./pay-item/pay-item.routes'),
  },
  {
    path: 'pay-invoice',
    data: { pageTitle: 'realTimeBeveragesApp.adminAuthority.home.title' },
    loadChildren: () => import('./pay-invoice/pay-invoice.routes'),
  },
  /* jhipster-needle-add-entity-route - JHipster will add entity modules routes here */
];

export default routes;
