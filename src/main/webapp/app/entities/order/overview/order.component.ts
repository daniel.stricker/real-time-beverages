import { AlertErrorComponent } from '../../../shared/alert/alert-error.component';
import TranslateDirective from '../../../shared/language/translate.directive';
import { SortByDirective, SortDirective } from '../../../shared/sort';
import { DurationPipe, FormatMediumDatePipe, FormatMediumDatetimePipe } from '../../../shared/date';
import { Component, computed, inject, signal } from '@angular/core';
import SharedModule from '../../../shared/shared.module';
import { OrderFormComponent } from '../order-form/order-form.component';
import { IOfferedItem } from '../../offered-item/offered-item.model';
import { OrderService } from '../services/order.service';

export interface OrderEntity {
  item?: IOfferedItem;
  amount?: number;
  id: number;
}

@Component({
  selector: 'jhi-order',
  standalone: true,
  imports: [
    AlertErrorComponent,
    TranslateDirective,
    SortDirective,
    SortByDirective,
    DurationPipe,
    FormatMediumDatetimePipe,
    FormatMediumDatePipe,
    SharedModule,
    OrderFormComponent,
  ],
  templateUrl: './order.component.html',
  styleUrl: './order.component.scss',
})
export class OrderComponent {
  orderService = inject(OrderService);
  order = signal<OrderEntity[] | undefined>(undefined);
  entity = signal<OrderEntity | undefined>(undefined);
  isEditing = computed(() => {
    const order = this.order();
    const entity = this.entity();
    if (order && entity) return order.some(order => order.id === entity.id);
    else return false;
  });
  orderConfirm = signal(false);

  addOrderEntity() {
    this.orderConfirm.set(false);
    const order = this.order();
    if (order && order.length > 0) this.entity.set({ id: order[order.length - 1].id + 1 });
    else this.entity.set({ id: 0 });
  }

  abortOrder() {
    this.entity.set(undefined);
  }

  addOrPatchOrder(entity: any) {
    const order = this.order();
    if (order && order.length > 0) {
      if (this.isEditing())
        this.order.set(
          order.map(orderEntity => {
            if (orderEntity.id === entity.id) return entity;
            else return orderEntity;
          }),
        );
      else this.order.set([...order, { ...entity, id: order[order.length - 1].id + 1 }]);
    } else this.order.set([{ ...entity, id: 0 }]);
    this.entity.set(undefined);
  }

  editEntity(id: number) {
    const order = this.order();
    if (order) this.entity.set(order.filter(entity => entity.id === id)[0]);
  }

  deleteEntity(id: number) {
    const order = this.order();
    if (order) this.order.set(order.filter(entity => entity.id !== id));
  }

  // Hinzufügen der Order
  addOrder() {
    const order = this.order();
    if (order) {
      this.orderService
        .createOrder(
          order.map(order => ({
            offeredItemId: order.item?.id,
            amount: order.amount,
          })),
        )
        .subscribe(response => {
          if (response.status === 200) {
            this.order.set(undefined);
            this.orderConfirm.set(true);
          }
        });
    }
  }
}
