import { UserRouteAccessService } from '../../core/auth/user-route-access.service';
import { Routes } from '@angular/router';
import { OrderComponent } from './overview/order.component';
import offeredItemRoute from '../offered-item/offered-item.routes';

const orderRoutes: Routes = [
  {
    path: '',
    component: OrderComponent,
    canActivate: [UserRouteAccessService],
  },
];

export default orderRoutes;
