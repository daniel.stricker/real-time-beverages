import { IOfferedItem, NewOfferedItem } from './offered-item.model';

export const sampleWithRequiredData: IOfferedItem = {
  id: 10086,
  name: 'predigen verantwortungsvoll',
  stock: 14786,
  price: 23915.84,
  hidden: true,
  limitedStockThreshold: 19738,
};

export const sampleWithPartialData: IOfferedItem = {
  id: 16755,
  name: 'beflügelnd',
  stock: 19390,
  price: 12970.22,
  hidden: true,
  limitedStockThreshold: 31809,
  category: 'ob',
};

export const sampleWithFullData: IOfferedItem = {
  id: 25581,
  name: 'brr likewise Penis',
  stock: 32450,
  price: 19132.66,
  hidden: true,
  image: '../fake-data/blob/hipster.png',
  imageContentType: 'unknown',
  limitedStockThreshold: 6150,
  category: 'je andantino spritzig',
};

export const sampleWithNewData: NewOfferedItem = {
  name: 'yowza however verkehrt',
  stock: 10391,
  price: 27284.18,
  hidden: true,
  limitedStockThreshold: 20002,
  id: null,
};

Object.freeze(sampleWithNewData);
Object.freeze(sampleWithRequiredData);
Object.freeze(sampleWithPartialData);
Object.freeze(sampleWithFullData);
