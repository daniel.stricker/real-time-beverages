import { Routes } from '@angular/router';

import { UserRouteAccessService } from 'app/core/auth/user-route-access.service';
import { ASC } from 'app/config/navigation.constants';
import { OfferedItemComponent } from './list/offered-item.component';
import { OfferedItemDetailComponent } from './detail/offered-item-detail.component';
import { OfferedItemUpdateComponent } from './update/offered-item-update.component';
import OfferedItemResolve from './route/offered-item-routing-resolve.service';

const offeredItemRoute: Routes = [
  {
    path: '',
    component: OfferedItemComponent,
    data: {
      defaultSort: 'id,' + ASC,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/view',
    component: OfferedItemDetailComponent,
    resolve: {
      offeredItem: OfferedItemResolve,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: 'new',
    component: OfferedItemUpdateComponent,
    resolve: {
      offeredItem: OfferedItemResolve,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/edit',
    component: OfferedItemUpdateComponent,
    resolve: {
      offeredItem: OfferedItemResolve,
    },
    canActivate: [UserRouteAccessService],
  },
];

export default offeredItemRoute;
