import { Component, inject, OnInit, ElementRef } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { finalize } from 'rxjs/operators';

import SharedModule from 'app/shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { AlertError } from 'app/shared/alert/alert-error.model';
import { EventManager, EventWithContent } from 'app/core/util/event-manager.service';
import { DataUtils, FileLoadError } from 'app/core/util/data-util.service';
import { OfferedItemService } from '../service/offered-item.service';
import { IOfferedItem } from '../offered-item.model';
import { OfferedItemFormService, OfferedItemFormGroup } from './offered-item-form.service';

@Component({
  standalone: true,
  selector: 'jhi-offered-item-update',
  templateUrl: './offered-item-update.component.html',
  imports: [SharedModule, FormsModule, ReactiveFormsModule],
})
export class OfferedItemUpdateComponent implements OnInit {
  isSaving = false;
  offeredItem: IOfferedItem | null = null;

  protected dataUtils = inject(DataUtils);
  protected eventManager = inject(EventManager);
  protected offeredItemService = inject(OfferedItemService);
  protected offeredItemFormService = inject(OfferedItemFormService);
  protected elementRef = inject(ElementRef);
  protected activatedRoute = inject(ActivatedRoute);

  // eslint-disable-next-line @typescript-eslint/member-ordering
  editForm: OfferedItemFormGroup = this.offeredItemFormService.createOfferedItemFormGroup();

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ offeredItem }) => {
      this.offeredItem = offeredItem;
      if (offeredItem) {
        this.updateForm(offeredItem);
      }
    });
  }

  byteSize(base64String: string): string {
    return this.dataUtils.byteSize(base64String);
  }

  openFile(base64String: string, contentType: string | null | undefined): void {
    this.dataUtils.openFile(base64String, contentType);
  }

  setFileData(event: Event, field: string, isImage: boolean): void {
    this.dataUtils.loadFileToForm(event, this.editForm, field, isImage).subscribe({
      error: (err: FileLoadError) =>
        this.eventManager.broadcast(
          new EventWithContent<AlertError>('realTimeBeveragesApp.error', { ...err, key: 'error.file.' + err.key }),
        ),
    });
  }

  clearInputImage(field: string, fieldContentType: string, idInput: string): void {
    this.editForm.patchValue({
      [field]: null,
      [fieldContentType]: null,
    });
    if (idInput && this.elementRef.nativeElement.querySelector('#' + idInput)) {
      this.elementRef.nativeElement.querySelector('#' + idInput).value = null;
    }
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const offeredItem = this.offeredItemFormService.getOfferedItem(this.editForm);
    if (offeredItem.id !== null) {
      this.subscribeToSaveResponse(this.offeredItemService.update(offeredItem));
    } else {
      this.subscribeToSaveResponse(this.offeredItemService.create(offeredItem));
    }
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IOfferedItem>>): void {
    result.pipe(finalize(() => this.onSaveFinalize())).subscribe({
      next: () => this.onSaveSuccess(),
      error: () => this.onSaveError(),
    });
  }

  protected onSaveSuccess(): void {
    this.previousState();
  }

  protected onSaveError(): void {
    // Api for inheritance.
  }

  protected onSaveFinalize(): void {
    this.isSaving = false;
  }

  protected updateForm(offeredItem: IOfferedItem): void {
    this.offeredItem = offeredItem;
    this.offeredItemFormService.resetForm(this.editForm, offeredItem);
  }
}
