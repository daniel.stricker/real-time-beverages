import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { of, Subject, from } from 'rxjs';

import { OfferedItemService } from '../service/offered-item.service';
import { IOfferedItem } from '../offered-item.model';
import { OfferedItemFormService } from './offered-item-form.service';

import { OfferedItemUpdateComponent } from './offered-item-update.component';

describe('OfferedItem Management Update Component', () => {
  let comp: OfferedItemUpdateComponent;
  let fixture: ComponentFixture<OfferedItemUpdateComponent>;
  let activatedRoute: ActivatedRoute;
  let offeredItemFormService: OfferedItemFormService;
  let offeredItemService: OfferedItemService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, OfferedItemUpdateComponent],
      providers: [
        FormBuilder,
        {
          provide: ActivatedRoute,
          useValue: {
            params: from([{}]),
          },
        },
      ],
    })
      .overrideTemplate(OfferedItemUpdateComponent, '')
      .compileComponents();

    fixture = TestBed.createComponent(OfferedItemUpdateComponent);
    activatedRoute = TestBed.inject(ActivatedRoute);
    offeredItemFormService = TestBed.inject(OfferedItemFormService);
    offeredItemService = TestBed.inject(OfferedItemService);

    comp = fixture.componentInstance;
  });

  describe('ngOnInit', () => {
    it('Should update editForm', () => {
      const offeredItem: IOfferedItem = { id: 456 };

      activatedRoute.data = of({ offeredItem });
      comp.ngOnInit();

      expect(comp.offeredItem).toEqual(offeredItem);
    });
  });

  describe('save', () => {
    it('Should call update service on save for existing entity', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<IOfferedItem>>();
      const offeredItem = { id: 123 };
      jest.spyOn(offeredItemFormService, 'getOfferedItem').mockReturnValue(offeredItem);
      jest.spyOn(offeredItemService, 'update').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ offeredItem });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.next(new HttpResponse({ body: offeredItem }));
      saveSubject.complete();

      // THEN
      expect(offeredItemFormService.getOfferedItem).toHaveBeenCalled();
      expect(comp.previousState).toHaveBeenCalled();
      expect(offeredItemService.update).toHaveBeenCalledWith(expect.objectContaining(offeredItem));
      expect(comp.isSaving).toEqual(false);
    });

    it('Should call create service on save for new entity', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<IOfferedItem>>();
      const offeredItem = { id: 123 };
      jest.spyOn(offeredItemFormService, 'getOfferedItem').mockReturnValue({ id: null });
      jest.spyOn(offeredItemService, 'create').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ offeredItem: null });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.next(new HttpResponse({ body: offeredItem }));
      saveSubject.complete();

      // THEN
      expect(offeredItemFormService.getOfferedItem).toHaveBeenCalled();
      expect(offeredItemService.create).toHaveBeenCalled();
      expect(comp.isSaving).toEqual(false);
      expect(comp.previousState).toHaveBeenCalled();
    });

    it('Should set isSaving to false on error', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<IOfferedItem>>();
      const offeredItem = { id: 123 };
      jest.spyOn(offeredItemService, 'update').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ offeredItem });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.error('This is an error!');

      // THEN
      expect(offeredItemService.update).toHaveBeenCalled();
      expect(comp.isSaving).toEqual(false);
      expect(comp.previousState).not.toHaveBeenCalled();
    });
  });
});
