import { Injectable } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';

import { IOfferedItem, NewOfferedItem } from '../offered-item.model';

/**
 * A partial Type with required key is used as form input.
 */
type PartialWithRequiredKeyOf<T extends { id: unknown }> = Partial<Omit<T, 'id'>> & { id: T['id'] };

/**
 * Type for createFormGroup and resetForm argument.
 * It accepts IOfferedItem for edit and NewOfferedItemFormGroupInput for create.
 */
type OfferedItemFormGroupInput = IOfferedItem | PartialWithRequiredKeyOf<NewOfferedItem>;

type OfferedItemFormDefaults = Pick<NewOfferedItem, 'id' | 'hidden'>;

type OfferedItemFormGroupContent = {
  id: FormControl<IOfferedItem['id'] | NewOfferedItem['id']>;
  name: FormControl<IOfferedItem['name']>;
  stock: FormControl<IOfferedItem['stock']>;
  price: FormControl<IOfferedItem['price']>;
  hidden: FormControl<IOfferedItem['hidden']>;
  image: FormControl<IOfferedItem['image']>;
  imageContentType: FormControl<IOfferedItem['imageContentType']>;
  limitedStockThreshold: FormControl<IOfferedItem['limitedStockThreshold']>;
  category: FormControl<IOfferedItem['category']>;
};

export type OfferedItemFormGroup = FormGroup<OfferedItemFormGroupContent>;

@Injectable({ providedIn: 'root' })
export class OfferedItemFormService {
  createOfferedItemFormGroup(offeredItem: OfferedItemFormGroupInput = { id: null }): OfferedItemFormGroup {
    const offeredItemRawValue = {
      ...this.getFormDefaults(),
      ...offeredItem,
    };
    return new FormGroup<OfferedItemFormGroupContent>({
      id: new FormControl(
        { value: offeredItemRawValue.id, disabled: true },
        {
          nonNullable: true,
          validators: [Validators.required],
        },
      ),
      name: new FormControl(offeredItemRawValue.name, {
        validators: [Validators.required, Validators.minLength(1)],
      }),
      stock: new FormControl(offeredItemRawValue.stock, {
        validators: [Validators.required, Validators.min(0)],
      }),
      price: new FormControl(offeredItemRawValue.price, {
        validators: [Validators.required, Validators.min(0)],
      }),
      hidden: new FormControl(offeredItemRawValue.hidden, {
        validators: [Validators.required],
      }),
      image: new FormControl(offeredItemRawValue.image),
      imageContentType: new FormControl(offeredItemRawValue.imageContentType),
      limitedStockThreshold: new FormControl(offeredItemRawValue.limitedStockThreshold, {
        validators: [Validators.required, Validators.min(0)],
      }),
      category: new FormControl(offeredItemRawValue.category, {
        validators: [Validators.minLength(1)],
      }),
    });
  }

  getOfferedItem(form: OfferedItemFormGroup): IOfferedItem | NewOfferedItem {
    return form.getRawValue() as IOfferedItem | NewOfferedItem;
  }

  resetForm(form: OfferedItemFormGroup, offeredItem: OfferedItemFormGroupInput): void {
    const offeredItemRawValue = { ...this.getFormDefaults(), ...offeredItem };
    form.reset(
      {
        ...offeredItemRawValue,
        id: { value: offeredItemRawValue.id, disabled: true },
      } as any /* cast to workaround https://github.com/angular/angular/issues/46458 */,
    );
  }

  private getFormDefaults(): OfferedItemFormDefaults {
    return {
      id: null,
      hidden: false,
    };
  }
}
