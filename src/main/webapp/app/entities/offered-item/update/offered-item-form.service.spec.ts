import { TestBed } from '@angular/core/testing';

import { sampleWithRequiredData, sampleWithNewData } from '../offered-item.test-samples';

import { OfferedItemFormService } from './offered-item-form.service';

describe('OfferedItem Form Service', () => {
  let service: OfferedItemFormService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(OfferedItemFormService);
  });

  describe('Service methods', () => {
    describe('createOfferedItemFormGroup', () => {
      it('should create a new form with FormControl', () => {
        const formGroup = service.createOfferedItemFormGroup();

        expect(formGroup.controls).toEqual(
          expect.objectContaining({
            id: expect.any(Object),
            name: expect.any(Object),
            stock: expect.any(Object),
            price: expect.any(Object),
            hidden: expect.any(Object),
            image: expect.any(Object),
            limitedStockThreshold: expect.any(Object),
            category: expect.any(Object),
          }),
        );
      });

      it('passing IOfferedItem should create a new form with FormGroup', () => {
        const formGroup = service.createOfferedItemFormGroup(sampleWithRequiredData);

        expect(formGroup.controls).toEqual(
          expect.objectContaining({
            id: expect.any(Object),
            name: expect.any(Object),
            stock: expect.any(Object),
            price: expect.any(Object),
            hidden: expect.any(Object),
            image: expect.any(Object),
            limitedStockThreshold: expect.any(Object),
            category: expect.any(Object),
          }),
        );
      });
    });

    describe('getOfferedItem', () => {
      it('should return NewOfferedItem for default OfferedItem initial value', () => {
        const formGroup = service.createOfferedItemFormGroup(sampleWithNewData);

        const offeredItem = service.getOfferedItem(formGroup) as any;

        expect(offeredItem).toMatchObject(sampleWithNewData);
      });

      it('should return NewOfferedItem for empty OfferedItem initial value', () => {
        const formGroup = service.createOfferedItemFormGroup();

        const offeredItem = service.getOfferedItem(formGroup) as any;

        expect(offeredItem).toMatchObject({});
      });

      it('should return IOfferedItem', () => {
        const formGroup = service.createOfferedItemFormGroup(sampleWithRequiredData);

        const offeredItem = service.getOfferedItem(formGroup) as any;

        expect(offeredItem).toMatchObject(sampleWithRequiredData);
      });
    });

    describe('resetForm', () => {
      it('passing IOfferedItem should not enable id FormControl', () => {
        const formGroup = service.createOfferedItemFormGroup();
        expect(formGroup.controls.id.disabled).toBe(true);

        service.resetForm(formGroup, sampleWithRequiredData);

        expect(formGroup.controls.id.disabled).toBe(true);
      });

      it('passing NewOfferedItem should disable id FormControl', () => {
        const formGroup = service.createOfferedItemFormGroup(sampleWithRequiredData);
        expect(formGroup.controls.id.disabled).toBe(true);

        service.resetForm(formGroup, { id: null });

        expect(formGroup.controls.id.disabled).toBe(true);
      });
    });
  });
});
