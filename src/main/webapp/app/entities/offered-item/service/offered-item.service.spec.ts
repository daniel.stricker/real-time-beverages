import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';

import { IOfferedItem } from '../offered-item.model';
import { sampleWithRequiredData, sampleWithNewData, sampleWithPartialData, sampleWithFullData } from '../offered-item.test-samples';

import { OfferedItemService } from './offered-item.service';

const requireRestSample: IOfferedItem = {
  ...sampleWithRequiredData,
};

describe('OfferedItem Service', () => {
  let service: OfferedItemService;
  let httpMock: HttpTestingController;
  let expectedResult: IOfferedItem | IOfferedItem[] | boolean | null;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
    });
    expectedResult = null;
    service = TestBed.inject(OfferedItemService);
    httpMock = TestBed.inject(HttpTestingController);
  });

  describe('Service methods', () => {
    it('should find an element', () => {
      const returnedFromService = { ...requireRestSample };
      const expected = { ...sampleWithRequiredData };

      service.find(123).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'GET' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should create a OfferedItem', () => {
      const offeredItem = { ...sampleWithNewData };
      const returnedFromService = { ...requireRestSample };
      const expected = { ...sampleWithRequiredData };

      service.create(offeredItem).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'POST' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should update a OfferedItem', () => {
      const offeredItem = { ...sampleWithRequiredData };
      const returnedFromService = { ...requireRestSample };
      const expected = { ...sampleWithRequiredData };

      service.update(offeredItem).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'PUT' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should partial update a OfferedItem', () => {
      const patchObject = { ...sampleWithPartialData };
      const returnedFromService = { ...requireRestSample };
      const expected = { ...sampleWithRequiredData };

      service.partialUpdate(patchObject).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'PATCH' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should return a list of OfferedItem', () => {
      const returnedFromService = { ...requireRestSample };

      const expected = { ...sampleWithRequiredData };

      service.query().subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'GET' });
      req.flush([returnedFromService]);
      httpMock.verify();
      expect(expectedResult).toMatchObject([expected]);
    });

    it('should delete a OfferedItem', () => {
      const expected = true;

      service.delete(123).subscribe(resp => (expectedResult = resp.ok));

      const req = httpMock.expectOne({ method: 'DELETE' });
      req.flush({ status: 200 });
      expect(expectedResult).toBe(expected);
    });

    describe('addOfferedItemToCollectionIfMissing', () => {
      it('should add a OfferedItem to an empty array', () => {
        const offeredItem: IOfferedItem = sampleWithRequiredData;
        expectedResult = service.addOfferedItemToCollectionIfMissing([], offeredItem);
        expect(expectedResult).toHaveLength(1);
        expect(expectedResult).toContain(offeredItem);
      });

      it('should not add a OfferedItem to an array that contains it', () => {
        const offeredItem: IOfferedItem = sampleWithRequiredData;
        const offeredItemCollection: IOfferedItem[] = [
          {
            ...offeredItem,
          },
          sampleWithPartialData,
        ];
        expectedResult = service.addOfferedItemToCollectionIfMissing(offeredItemCollection, offeredItem);
        expect(expectedResult).toHaveLength(2);
      });

      it("should add a OfferedItem to an array that doesn't contain it", () => {
        const offeredItem: IOfferedItem = sampleWithRequiredData;
        const offeredItemCollection: IOfferedItem[] = [sampleWithPartialData];
        expectedResult = service.addOfferedItemToCollectionIfMissing(offeredItemCollection, offeredItem);
        expect(expectedResult).toHaveLength(2);
        expect(expectedResult).toContain(offeredItem);
      });

      it('should add only unique OfferedItem to an array', () => {
        const offeredItemArray: IOfferedItem[] = [sampleWithRequiredData, sampleWithPartialData, sampleWithFullData];
        const offeredItemCollection: IOfferedItem[] = [sampleWithRequiredData];
        expectedResult = service.addOfferedItemToCollectionIfMissing(offeredItemCollection, ...offeredItemArray);
        expect(expectedResult).toHaveLength(3);
      });

      it('should accept varargs', () => {
        const offeredItem: IOfferedItem = sampleWithRequiredData;
        const offeredItem2: IOfferedItem = sampleWithPartialData;
        expectedResult = service.addOfferedItemToCollectionIfMissing([], offeredItem, offeredItem2);
        expect(expectedResult).toHaveLength(2);
        expect(expectedResult).toContain(offeredItem);
        expect(expectedResult).toContain(offeredItem2);
      });

      it('should accept null and undefined values', () => {
        const offeredItem: IOfferedItem = sampleWithRequiredData;
        expectedResult = service.addOfferedItemToCollectionIfMissing([], null, offeredItem, undefined);
        expect(expectedResult).toHaveLength(1);
        expect(expectedResult).toContain(offeredItem);
      });

      it('should return initial array if no OfferedItem is added', () => {
        const offeredItemCollection: IOfferedItem[] = [sampleWithRequiredData];
        expectedResult = service.addOfferedItemToCollectionIfMissing(offeredItemCollection, undefined, null);
        expect(expectedResult).toEqual(offeredItemCollection);
      });
    });

    describe('compareOfferedItem', () => {
      it('Should return true if both entities are null', () => {
        const entity1 = null;
        const entity2 = null;

        const compareResult = service.compareOfferedItem(entity1, entity2);

        expect(compareResult).toEqual(true);
      });

      it('Should return false if one entity is null', () => {
        const entity1 = { id: 123 };
        const entity2 = null;

        const compareResult1 = service.compareOfferedItem(entity1, entity2);
        const compareResult2 = service.compareOfferedItem(entity2, entity1);

        expect(compareResult1).toEqual(false);
        expect(compareResult2).toEqual(false);
      });

      it('Should return false if primaryKey differs', () => {
        const entity1 = { id: 123 };
        const entity2 = { id: 456 };

        const compareResult1 = service.compareOfferedItem(entity1, entity2);
        const compareResult2 = service.compareOfferedItem(entity2, entity1);

        expect(compareResult1).toEqual(false);
        expect(compareResult2).toEqual(false);
      });

      it('Should return false if primaryKey matches', () => {
        const entity1 = { id: 123 };
        const entity2 = { id: 123 };

        const compareResult1 = service.compareOfferedItem(entity1, entity2);
        const compareResult2 = service.compareOfferedItem(entity2, entity1);

        expect(compareResult1).toEqual(true);
        expect(compareResult2).toEqual(true);
      });
    });
  });

  afterEach(() => {
    httpMock.verify();
  });
});
