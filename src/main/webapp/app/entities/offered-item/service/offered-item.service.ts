import { inject, Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { isPresent } from 'app/core/util/operators';
import { ApplicationConfigService } from 'app/core/config/application-config.service';
import { createRequestOption } from 'app/core/request/request-util';
import { IOfferedItem, NewOfferedItem } from '../offered-item.model';

export type PartialUpdateOfferedItem = Partial<IOfferedItem> & Pick<IOfferedItem, 'id'>;

export type EntityResponseType = HttpResponse<IOfferedItem>;
export type EntityArrayResponseType = HttpResponse<IOfferedItem[]>;

@Injectable({ providedIn: 'root' })
export class OfferedItemService {
  protected http = inject(HttpClient);
  protected applicationConfigService = inject(ApplicationConfigService);

  protected resourceUrl = this.applicationConfigService.getEndpointFor('api/offered-items');

  create(offeredItem: NewOfferedItem): Observable<EntityResponseType> {
    return this.http.post<IOfferedItem>(this.resourceUrl, offeredItem, { observe: 'response' });
  }

  update(offeredItem: IOfferedItem): Observable<EntityResponseType> {
    return this.http.put<IOfferedItem>(`${this.resourceUrl}/${this.getOfferedItemIdentifier(offeredItem)}`, offeredItem, {
      observe: 'response',
    });
  }

  partialUpdate(offeredItem: PartialUpdateOfferedItem): Observable<EntityResponseType> {
    return this.http.patch<IOfferedItem>(`${this.resourceUrl}/${this.getOfferedItemIdentifier(offeredItem)}`, offeredItem, {
      observe: 'response',
    });
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<IOfferedItem>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IOfferedItem[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  getOfferedItemIdentifier(offeredItem: Pick<IOfferedItem, 'id'>): number {
    return offeredItem.id;
  }

  compareOfferedItem(o1: Pick<IOfferedItem, 'id'> | null, o2: Pick<IOfferedItem, 'id'> | null): boolean {
    return o1 && o2 ? this.getOfferedItemIdentifier(o1) === this.getOfferedItemIdentifier(o2) : o1 === o2;
  }

  addOfferedItemToCollectionIfMissing<Type extends Pick<IOfferedItem, 'id'>>(
    offeredItemCollection: Type[],
    ...offeredItemsToCheck: (Type | null | undefined)[]
  ): Type[] {
    const offeredItems: Type[] = offeredItemsToCheck.filter(isPresent);
    if (offeredItems.length > 0) {
      const offeredItemCollectionIdentifiers = offeredItemCollection.map(offeredItemItem => this.getOfferedItemIdentifier(offeredItemItem));
      const offeredItemsToAdd = offeredItems.filter(offeredItemItem => {
        const offeredItemIdentifier = this.getOfferedItemIdentifier(offeredItemItem);
        if (offeredItemCollectionIdentifiers.includes(offeredItemIdentifier)) {
          return false;
        }
        offeredItemCollectionIdentifiers.push(offeredItemIdentifier);
        return true;
      });
      return [...offeredItemsToAdd, ...offeredItemCollection];
    }
    return offeredItemCollection;
  }
}
