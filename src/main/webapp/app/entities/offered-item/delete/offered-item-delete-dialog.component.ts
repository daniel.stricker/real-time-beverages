import { Component, inject } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

import SharedModule from 'app/shared/shared.module';
import { ITEM_DELETED_EVENT } from 'app/config/navigation.constants';
import { IOfferedItem } from '../offered-item.model';
import { OfferedItemService } from '../service/offered-item.service';

@Component({
  standalone: true,
  templateUrl: './offered-item-delete-dialog.component.html',
  imports: [SharedModule, FormsModule],
})
export class OfferedItemDeleteDialogComponent {
  offeredItem?: IOfferedItem;

  protected offeredItemService = inject(OfferedItemService);
  protected activeModal = inject(NgbActiveModal);

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.offeredItemService.delete(id).subscribe(() => {
      this.activeModal.close(ITEM_DELETED_EVENT);
    });
  }
}
