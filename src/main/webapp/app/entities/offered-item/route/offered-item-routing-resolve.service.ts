import { inject } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { ActivatedRouteSnapshot, Router } from '@angular/router';
import { of, EMPTY, Observable } from 'rxjs';
import { mergeMap } from 'rxjs/operators';

import { IOfferedItem } from '../offered-item.model';
import { OfferedItemService } from '../service/offered-item.service';

const offeredItemResolve = (route: ActivatedRouteSnapshot): Observable<null | IOfferedItem> => {
  const id = route.params['id'];
  if (id) {
    return inject(OfferedItemService)
      .find(id)
      .pipe(
        mergeMap((offeredItem: HttpResponse<IOfferedItem>) => {
          if (offeredItem.body) {
            return of(offeredItem.body);
          } else {
            inject(Router).navigate(['404']);
            return EMPTY;
          }
        }),
      );
  }
  return of(null);
};

export default offeredItemResolve;
