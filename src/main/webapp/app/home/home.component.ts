import { Component, computed, effect, inject, OnDestroy, OnInit, signal } from '@angular/core';
import { Router, RouterModule } from '@angular/router';
import { Observable, Subject } from 'rxjs';
import { map, takeUntil } from 'rxjs/operators';

import SharedModule from 'app/shared/shared.module';
import { AccountService } from 'app/core/auth/account.service';
import { Account } from 'app/core/auth/account.model';
import { SortByDirective, SortDirective } from '../shared/sort';
import { OfferedItemService } from '../entities/offered-item/service/offered-item.service';
import { Authority } from '../config/authority.constants';
import { IOfferedItem } from '../entities/offered-item/offered-item.model';
import { IBoughtItem } from '../entities/bought-item/bought-item.model';
import { BoughtItemService } from '../entities/bought-item/service/bought-item.service';
import { IUser } from '../entities/user/user.model';

@Component({
  standalone: true,
  selector: 'jhi-home',
  templateUrl: './home.component.html',
  styleUrl: './home.component.scss',
  imports: [SharedModule, RouterModule, SortByDirective, SortDirective],
})
export default class HomeComponent implements OnInit, OnDestroy {
  account = signal<Account | null>(null);
  offeredItemService = inject(OfferedItemService);
  boughtItemService = inject(BoughtItemService);
  isAdmin = computed(() => this.account()?.authorities.includes(Authority.ADMIN));
  almostEmptyOfferedItems$!: Observable<IOfferedItem[] | null | undefined>;
  recentlyBoughtItems$!: Observable<IBoughtItem[] | null | undefined>;
  private readonly destroy$ = new Subject<void>();
  private accountService = inject(AccountService);
  private router = inject(Router);

  constructor() {
    effect(() => {
      if (this.account())
        this.recentlyBoughtItems$ = this.boughtItemService.query().pipe(
          map(responses => {
            const entities = responses.body;
            return entities
              ?.filter(entities => {
                const entityBuyer = entities.buyer as IUser;
                return entityBuyer.login === this.account()?.login;
              })
              .sort((entity1, entity2) => entity2.boughtTime!.toDate().getDate() - entity1.boughtTime!.toDate().getDate())
              .slice(0, 4);
          }),
        );

      if (this.isAdmin())
        this.almostEmptyOfferedItems$ = this.offeredItemService.query().pipe(
          map(responses => {
            const entities = responses.body;
            const almostEmptyOfferedItems = entities?.filter(entities => entities.stock! < entities.limitedStockThreshold!);
            return almostEmptyOfferedItems && almostEmptyOfferedItems.length > 0 ? almostEmptyOfferedItems : null;
          }),
        );
    });
  }

  ngOnInit(): void {
    this.accountService
      .getAuthenticationState()
      .pipe(takeUntil(this.destroy$))
      .subscribe(account => this.account.set(account));
  }

  login(): void {
    this.router.navigate(['/login']);
  }

  ngOnDestroy(): void {
    this.destroy$.next();
    this.destroy$.complete();
  }
}
