package de.tudarmstadt.realtimebeverages.domain;

import jakarta.persistence.*;
import jakarta.validation.constraints.*;
import java.io.Serializable;
import java.math.BigDecimal;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

/**
 * A OfferedItem.
 */
@Entity
@Table(name = "offered_item")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@SuppressWarnings("common-java:DuplicatedBlocks")
public class OfferedItem implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    @Column(name = "id")
    private Long id;

    @NotNull
    @Size(min = 1)
    @Column(name = "name", nullable = false, unique = true)
    private String name;

    @NotNull
    @Min(value = 0)
    @Column(name = "stock", nullable = false)
    private Integer stock;

    @NotNull
    @DecimalMin(value = "0")
    @Column(name = "price", precision = 21, scale = 2, nullable = false)
    private BigDecimal price;

    @NotNull
    @Column(name = "hidden", nullable = false)
    private Boolean hidden;

    @Lob
    @Column(name = "image")
    private byte[] image;

    @Column(name = "image_content_type")
    private String imageContentType;

    @NotNull
    @Min(value = 0)
    @Column(name = "limited_stock_threshold", nullable = false)
    private Integer limitedStockThreshold;

    @Size(min = 1)
    @Column(name = "category")
    private String category;

    // jhipster-needle-entity-add-field - JHipster will add fields here

    public Long getId() {
        return this.id;
    }

    public OfferedItem id(Long id) {
        this.setId(id);
        return this;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return this.name;
    }

    public OfferedItem name(String name) {
        this.setName(name);
        return this;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getStock() {
        return this.stock;
    }

    public OfferedItem stock(Integer stock) {
        this.setStock(stock);
        return this;
    }

    public void setStock(Integer stock) {
        this.stock = stock;
    }

    public BigDecimal getPrice() {
        return this.price;
    }

    public OfferedItem price(BigDecimal price) {
        this.setPrice(price);
        return this;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public Boolean getHidden() {
        return this.hidden;
    }

    public OfferedItem hidden(Boolean hidden) {
        this.setHidden(hidden);
        return this;
    }

    public void setHidden(Boolean hidden) {
        this.hidden = hidden;
    }

    public byte[] getImage() {
        return this.image;
    }

    public OfferedItem image(byte[] image) {
        this.setImage(image);
        return this;
    }

    public void setImage(byte[] image) {
        this.image = image;
    }

    public String getImageContentType() {
        return this.imageContentType;
    }

    public OfferedItem imageContentType(String imageContentType) {
        this.imageContentType = imageContentType;
        return this;
    }

    public void setImageContentType(String imageContentType) {
        this.imageContentType = imageContentType;
    }

    public Integer getLimitedStockThreshold() {
        return this.limitedStockThreshold;
    }

    public OfferedItem limitedStockThreshold(Integer limitedStockThreshold) {
        this.setLimitedStockThreshold(limitedStockThreshold);
        return this;
    }

    public void setLimitedStockThreshold(Integer limitedStockThreshold) {
        this.limitedStockThreshold = limitedStockThreshold;
    }

    public String getCategory() {
        return this.category;
    }

    public OfferedItem category(String category) {
        this.setCategory(category);
        return this;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof OfferedItem)) {
            return false;
        }
        return getId() != null && getId().equals(((OfferedItem) o).getId());
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "OfferedItem{" +
            "id=" + getId() +
            ", name='" + getName() + "'" +
            ", stock=" + getStock() +
            ", price=" + getPrice() +
            ", hidden='" + getHidden() + "'" +
            ", image='" + getImage() + "'" +
            ", imageContentType='" + getImageContentType() + "'" +
            ", limitedStockThreshold=" + getLimitedStockThreshold() +
            ", category='" + getCategory() + "'" +
            "}";
    }
}
