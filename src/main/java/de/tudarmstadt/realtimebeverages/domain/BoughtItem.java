package de.tudarmstadt.realtimebeverages.domain;

import jakarta.persistence.*;
import jakarta.validation.constraints.DecimalMin;
import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;
import java.io.Serializable;
import java.math.BigDecimal;
import java.time.Instant;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

/**
 * A BoughtItem.
 */
@Entity
@Table(name = "bought_item")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@SuppressWarnings("common-java:DuplicatedBlocks")
public class BoughtItem implements Serializable {

    private static final long serialVersionUID = 1L;

    public BoughtItem() {}

    public BoughtItem(OfferedItem offer, int amount, User buyer) {
        this.name = offer.getName();
        this.price = offer.getPrice();
        this.boughtTime = Instant.now();
        this.amount = amount;
        this.offer = offer;
        this.buyer = buyer;
    }

    public BoughtItem(
        Long id,
        String name,
        BigDecimal price,
        Instant boughtTime,
        Instant billingTime,
        Instant paidTime,
        Integer amount,
        OfferedItem offer,
        User buyer
    ) {
        this.id = id;
        this.name = name;
        this.price = price;
        this.boughtTime = boughtTime;
        this.billingTime = billingTime;
        this.paidTime = paidTime;
        this.amount = amount;
        this.offer = offer;
        this.buyer = buyer;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    @Column(name = "id")
    private Long id;

    @NotNull
    @Size(min = 1)
    @Column(name = "name", nullable = false)
    private String name;

    @NotNull
    @DecimalMin(value = "0")
    @Column(name = "price", precision = 21, scale = 2, nullable = false)
    private BigDecimal price;

    @NotNull
    @Column(name = "bought_time", nullable = false)
    private Instant boughtTime;

    @Column(name = "billing_time")
    private Instant billingTime;

    @Column(name = "paid_time")
    private Instant paidTime;

    @NotNull
    @Min(value = 1)
    @Column(name = "amount", nullable = false)
    private Integer amount;

    @ManyToOne(optional = false)
    @NotNull
    private OfferedItem offer;

    @ManyToOne(optional = false)
    @NotNull
    private User buyer;

    // jhipster-needle-entity-add-field - JHipster will add fields here

    public Long getId() {
        return this.id;
    }

    public BoughtItem id(Long id) {
        this.setId(id);
        return this;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return this.name;
    }

    public BoughtItem name(String name) {
        this.setName(name);
        return this;
    }

    public void setName(String name) {
        this.name = name;
    }

    public BigDecimal getPrice() {
        return this.price;
    }

    public BoughtItem price(BigDecimal price) {
        this.setPrice(price);
        return this;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public Instant getBoughtTime() {
        return this.boughtTime;
    }

    public BoughtItem boughtTime(Instant boughtTime) {
        this.setBoughtTime(boughtTime);
        return this;
    }

    public void setBoughtTime(Instant boughtTime) {
        this.boughtTime = boughtTime;
    }

    public Instant getBillingTime() {
        return this.billingTime;
    }

    public BoughtItem billingTime(Instant billingTime) {
        this.setBillingTime(billingTime);
        return this;
    }

    public void setBillingTime(Instant billingTime) {
        this.billingTime = billingTime;
    }

    public Instant getPaidTime() {
        return this.paidTime;
    }

    public BoughtItem paidTime(Instant paidTime) {
        this.setPaidTime(paidTime);
        return this;
    }

    public void setPaidTime(Instant paidTime) {
        this.paidTime = paidTime;
    }

    public Integer getAmount() {
        return this.amount;
    }

    public BoughtItem amount(Integer amount) {
        this.setAmount(amount);
        return this;
    }

    public void setAmount(Integer amount) {
        this.amount = amount;
    }

    public OfferedItem getOffer() {
        return this.offer;
    }

    public void setOffer(OfferedItem offeredItem) {
        this.offer = offeredItem;
    }

    public BoughtItem offer(OfferedItem offeredItem) {
        this.setOffer(offeredItem);
        return this;
    }

    public User getBuyer() {
        return this.buyer;
    }

    public void setBuyer(User user) {
        this.buyer = user;
    }

    public BoughtItem buyer(User user) {
        this.setBuyer(user);
        return this;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof BoughtItem)) {
            return false;
        }
        return getId() != null && getId().equals(((BoughtItem) o).getId());
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "BoughtItem{" +
            "id=" + getId() +
            ", name='" + getName() + "'" +
            ", price=" + getPrice() +
            ", boughtTime='" + getBoughtTime() + "'" +
            ", billingTime='" + getBillingTime() + "'" +
            ", billingTime='" + getPaidTime() + "'" +
            ", amount=" + getAmount() +
            "}";
    }
}
