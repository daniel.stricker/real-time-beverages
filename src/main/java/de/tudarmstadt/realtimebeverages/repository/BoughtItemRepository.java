package de.tudarmstadt.realtimebeverages.repository;

import de.tudarmstadt.realtimebeverages.domain.BoughtItem;
import jakarta.validation.constraints.NotNull;
import java.time.Instant;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

/**
 * Spring Data JPA repository for the BoughtItem entity.
 */
@SuppressWarnings("unused")
@Repository
public interface BoughtItemRepository extends JpaRepository<BoughtItem, Long> {
    @Query("select boughtItem from BoughtItem boughtItem where boughtItem.buyer.login = ?#{authentication.name}")
    List<BoughtItem> findByBuyerIsCurrentUser();

    @Query(
        "select boughtItem from BoughtItem boughtItem where boughtItem.buyer.id = :buyerId and boughtItem.billingTime is null and boughtItem.boughtTime <= :boughtBeforeIncluding order by boughtItem.boughtTime"
    )
    List<BoughtItem> findUnbilledBeforeIncluding(
        @NotNull @Param("buyerId") Long buyerId,
        @NotNull @Param("boughtBeforeIncluding") Instant boughtBeforeIncluding
    );

    @Query(
        "select boughtItem from BoughtItem boughtItem where boughtItem.buyer.id = :buyerId and boughtItem.billingTime is not null and boughtItem.paidTime is null order by boughtItem.billingTime"
    )
    List<BoughtItem> findBilledNotPaidByBuyerId(@NotNull @Param("buyerId") Long buyerId);

    @Query(
        "select boughtItem from BoughtItem boughtItem where boughtItem.buyer.id = :buyerId and boughtItem.billingTime = :billingTime and boughtItem.paidTime is null"
    )
    List<BoughtItem> findBilledNotPaidByBuyerIdAndBillingTime(
        @NotNull @Param("buyerId") Long buyerId,
        @NotNull @Param("billingTime") Instant billingTime
    );
}
