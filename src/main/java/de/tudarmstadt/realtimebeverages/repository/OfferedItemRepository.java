package de.tudarmstadt.realtimebeverages.repository;

import de.tudarmstadt.realtimebeverages.domain.OfferedItem;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data JPA repository for the OfferedItem entity.
 */
@SuppressWarnings("unused")
@Repository
public interface OfferedItemRepository extends JpaRepository<OfferedItem, Long> {}
