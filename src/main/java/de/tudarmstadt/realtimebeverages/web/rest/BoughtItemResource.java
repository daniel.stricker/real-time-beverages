package de.tudarmstadt.realtimebeverages.web.rest;

import de.tudarmstadt.realtimebeverages.domain.BoughtItem;
import de.tudarmstadt.realtimebeverages.repository.BoughtItemRepository;
import de.tudarmstadt.realtimebeverages.web.rest.errors.BadRequestAlertException;
import jakarta.validation.Valid;
import jakarta.validation.constraints.NotNull;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing {@link de.tudarmstadt.realtimebeverages.domain.BoughtItem}.
 */
@RestController
@RequestMapping("/api/bought-items")
@Transactional
public class BoughtItemResource {

    private static final Logger log = LoggerFactory.getLogger(BoughtItemResource.class);

    private static final String ENTITY_NAME = "boughtItem";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final BoughtItemRepository boughtItemRepository;

    public BoughtItemResource(BoughtItemRepository boughtItemRepository) {
        this.boughtItemRepository = boughtItemRepository;
    }

    /**
     * {@code POST  /bought-items} : Create a new boughtItem.
     *
     * @param boughtItem the boughtItem to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new boughtItem, or with status {@code 400 (Bad Request)} if the boughtItem has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("")
    public ResponseEntity<BoughtItem> createBoughtItem(@Valid @RequestBody BoughtItem boughtItem) throws URISyntaxException {
        log.debug("REST request to save BoughtItem : {}", boughtItem);
        if (boughtItem.getId() != null) {
            throw new BadRequestAlertException("A new boughtItem cannot already have an ID", ENTITY_NAME, "idexists");
        }
        boughtItem = boughtItemRepository.save(boughtItem);
        return ResponseEntity.created(new URI("/api/bought-items/" + boughtItem.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, boughtItem.getId().toString()))
            .body(boughtItem);
    }

    /**
     * {@code PUT  /bought-items/:id} : Updates an existing boughtItem.
     *
     * @param id the id of the boughtItem to save.
     * @param boughtItem the boughtItem to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated boughtItem,
     * or with status {@code 400 (Bad Request)} if the boughtItem is not valid,
     * or with status {@code 500 (Internal Server Error)} if the boughtItem couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/{id}")
    public ResponseEntity<BoughtItem> updateBoughtItem(
        @PathVariable(value = "id", required = false) final Long id,
        @Valid @RequestBody BoughtItem boughtItem
    ) throws URISyntaxException {
        log.debug("REST request to update BoughtItem : {}, {}", id, boughtItem);
        if (boughtItem.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, boughtItem.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!boughtItemRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        boughtItem = boughtItemRepository.save(boughtItem);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, boughtItem.getId().toString()))
            .body(boughtItem);
    }

    /**
     * {@code PATCH  /bought-items/:id} : Partial updates given fields of an existing boughtItem, field will ignore if it is null
     *
     * @param id the id of the boughtItem to save.
     * @param boughtItem the boughtItem to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated boughtItem,
     * or with status {@code 400 (Bad Request)} if the boughtItem is not valid,
     * or with status {@code 404 (Not Found)} if the boughtItem is not found,
     * or with status {@code 500 (Internal Server Error)} if the boughtItem couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PatchMapping(value = "/{id}", consumes = { "application/json", "application/merge-patch+json" })
    public ResponseEntity<BoughtItem> partialUpdateBoughtItem(
        @PathVariable(value = "id", required = false) final Long id,
        @NotNull @RequestBody BoughtItem boughtItem
    ) throws URISyntaxException {
        log.debug("REST request to partial update BoughtItem partially : {}, {}", id, boughtItem);
        if (boughtItem.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, boughtItem.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!boughtItemRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Optional<BoughtItem> result = boughtItemRepository
            .findById(boughtItem.getId())
            .map(existingBoughtItem -> {
                if (boughtItem.getName() != null) {
                    existingBoughtItem.setName(boughtItem.getName());
                }
                if (boughtItem.getPrice() != null) {
                    existingBoughtItem.setPrice(boughtItem.getPrice());
                }
                if (boughtItem.getBoughtTime() != null) {
                    existingBoughtItem.setBoughtTime(boughtItem.getBoughtTime());
                }
                if (boughtItem.getBillingTime() != null) {
                    existingBoughtItem.setBillingTime(boughtItem.getBillingTime());
                }
                if (boughtItem.getPaidTime() != null) {
                    existingBoughtItem.setPaidTime(boughtItem.getPaidTime());
                }
                if (boughtItem.getAmount() != null) {
                    existingBoughtItem.setAmount(boughtItem.getAmount());
                }

                return existingBoughtItem;
            })
            .map(boughtItemRepository::save);

        return ResponseUtil.wrapOrNotFound(
            result,
            HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, boughtItem.getId().toString())
        );
    }

    /**
     * {@code GET  /bought-items} : get all the boughtItems.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of boughtItems in body.
     */
    @GetMapping("")
    public List<BoughtItem> getAllBoughtItems() {
        log.debug("REST request to get all BoughtItems");
        return boughtItemRepository.findAll();
    }

    /**
     * {@code GET  /bought-items/:id} : get the "id" boughtItem.
     *
     * @param id the id of the boughtItem to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the boughtItem, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/{id}")
    public ResponseEntity<BoughtItem> getBoughtItem(@PathVariable("id") Long id) {
        log.debug("REST request to get BoughtItem : {}", id);
        Optional<BoughtItem> boughtItem = boughtItemRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(boughtItem);
    }

    /**
     * {@code DELETE  /bought-items/:id} : delete the "id" boughtItem.
     *
     * @param id the id of the boughtItem to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deleteBoughtItem(@PathVariable("id") Long id) {
        log.debug("REST request to delete BoughtItem : {}", id);
        boughtItemRepository.deleteById(id);
        return ResponseEntity.noContent()
            .headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString()))
            .build();
    }
}
