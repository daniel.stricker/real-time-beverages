package de.tudarmstadt.realtimebeverages.web.rest;

import de.tudarmstadt.realtimebeverages.domain.OfferedItem;
import de.tudarmstadt.realtimebeverages.repository.OfferedItemRepository;
import de.tudarmstadt.realtimebeverages.web.rest.errors.BadRequestAlertException;
import jakarta.validation.Valid;
import jakarta.validation.constraints.NotNull;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing {@link de.tudarmstadt.realtimebeverages.domain.OfferedItem}.
 */
@RestController
@RequestMapping("/api/offered-items")
@Transactional
public class OfferedItemResource {

    private final Logger log = LoggerFactory.getLogger(OfferedItemResource.class);

    private static final String ENTITY_NAME = "offeredItem";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final OfferedItemRepository offeredItemRepository;

    public OfferedItemResource(OfferedItemRepository offeredItemRepository) {
        this.offeredItemRepository = offeredItemRepository;
    }

    /**
     * {@code POST  /offered-items} : Create a new offeredItem.
     *
     * @param offeredItem the offeredItem to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new offeredItem, or with status {@code 400 (Bad Request)} if the offeredItem has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("")
    public ResponseEntity<OfferedItem> createOfferedItem(@Valid @RequestBody OfferedItem offeredItem) throws URISyntaxException {
        log.debug("REST request to save OfferedItem : {}", offeredItem);
        if (offeredItem.getId() != null) {
            throw new BadRequestAlertException("A new offeredItem cannot already have an ID", ENTITY_NAME, "idexists");
        }
        offeredItem = offeredItemRepository.save(offeredItem);
        return ResponseEntity.created(new URI("/api/offered-items/" + offeredItem.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, offeredItem.getId().toString()))
            .body(offeredItem);
    }

    /**
     * {@code PUT  /offered-items/:id} : Updates an existing offeredItem.
     *
     * @param id the id of the offeredItem to save.
     * @param offeredItem the offeredItem to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated offeredItem,
     * or with status {@code 400 (Bad Request)} if the offeredItem is not valid,
     * or with status {@code 500 (Internal Server Error)} if the offeredItem couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/{id}")
    public ResponseEntity<OfferedItem> updateOfferedItem(
        @PathVariable(value = "id", required = false) final Long id,
        @Valid @RequestBody OfferedItem offeredItem
    ) throws URISyntaxException {
        log.debug("REST request to update OfferedItem : {}, {}", id, offeredItem);
        if (offeredItem.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, offeredItem.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!offeredItemRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        offeredItem = offeredItemRepository.save(offeredItem);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, offeredItem.getId().toString()))
            .body(offeredItem);
    }

    /**
     * {@code PATCH  /offered-items/:id} : Partial updates given fields of an existing offeredItem, field will ignore if it is null
     *
     * @param id the id of the offeredItem to save.
     * @param offeredItem the offeredItem to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated offeredItem,
     * or with status {@code 400 (Bad Request)} if the offeredItem is not valid,
     * or with status {@code 404 (Not Found)} if the offeredItem is not found,
     * or with status {@code 500 (Internal Server Error)} if the offeredItem couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PatchMapping(value = "/{id}", consumes = { "application/json", "application/merge-patch+json" })
    public ResponseEntity<OfferedItem> partialUpdateOfferedItem(
        @PathVariable(value = "id", required = false) final Long id,
        @NotNull @RequestBody OfferedItem offeredItem
    ) throws URISyntaxException {
        log.debug("REST request to partial update OfferedItem partially : {}, {}", id, offeredItem);
        if (offeredItem.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, offeredItem.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!offeredItemRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Optional<OfferedItem> result = offeredItemRepository
            .findById(offeredItem.getId())
            .map(existingOfferedItem -> {
                if (offeredItem.getName() != null) {
                    existingOfferedItem.setName(offeredItem.getName());
                }
                if (offeredItem.getStock() != null) {
                    existingOfferedItem.setStock(offeredItem.getStock());
                }
                if (offeredItem.getPrice() != null) {
                    existingOfferedItem.setPrice(offeredItem.getPrice());
                }
                if (offeredItem.getHidden() != null) {
                    existingOfferedItem.setHidden(offeredItem.getHidden());
                }
                if (offeredItem.getImage() != null) {
                    existingOfferedItem.setImage(offeredItem.getImage());
                }
                if (offeredItem.getImageContentType() != null) {
                    existingOfferedItem.setImageContentType(offeredItem.getImageContentType());
                }
                if (offeredItem.getLimitedStockThreshold() != null) {
                    existingOfferedItem.setLimitedStockThreshold(offeredItem.getLimitedStockThreshold());
                }
                if (offeredItem.getCategory() != null) {
                    existingOfferedItem.setCategory(offeredItem.getCategory());
                }

                return existingOfferedItem;
            })
            .map(offeredItemRepository::save);

        return ResponseUtil.wrapOrNotFound(
            result,
            HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, offeredItem.getId().toString())
        );
    }

    /**
     * {@code GET  /offered-items} : get all the offeredItems.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of offeredItems in body.
     */
    @GetMapping("")
    public List<OfferedItem> getAllOfferedItems() {
        log.debug("REST request to get all OfferedItems");
        return offeredItemRepository.findAll();
    }

    /**
     * {@code GET  /offered-items/:id} : get the "id" offeredItem.
     *
     * @param id the id of the offeredItem to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the offeredItem, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/{id}")
    public ResponseEntity<OfferedItem> getOfferedItem(@PathVariable("id") Long id) {
        log.debug("REST request to get OfferedItem : {}", id);
        Optional<OfferedItem> offeredItem = offeredItemRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(offeredItem);
    }

    /**
     * {@code DELETE  /offered-items/:id} : delete the "id" offeredItem.
     *
     * @param id the id of the offeredItem to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deleteOfferedItem(@PathVariable("id") Long id) {
        log.debug("REST request to delete OfferedItem : {}", id);
        offeredItemRepository.deleteById(id);
        return ResponseEntity.noContent()
            .headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString()))
            .build();
    }
}
