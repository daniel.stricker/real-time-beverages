package de.tudarmstadt.realtimebeverages.web.rest;

import de.tudarmstadt.realtimebeverages.domain.BoughtItem;
import de.tudarmstadt.realtimebeverages.domain.OfferedItem;
import de.tudarmstadt.realtimebeverages.repository.BoughtItemRepository;
import de.tudarmstadt.realtimebeverages.repository.OfferedItemRepository;
import de.tudarmstadt.realtimebeverages.service.UserService;
import de.tudarmstadt.realtimebeverages.web.rest.errors.BadRequestAlertException;
import jakarta.validation.Valid;
import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Positive;
import java.time.Instant;
import java.util.Comparator;
import java.util.List;
import java.util.Objects;
import java.util.function.BiConsumer;
import java.util.function.BiFunction;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.stream.Collectors;
import org.jooq.lambda.Unchecked;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

/**
 * Because this is {@link Transactional} changes to the database in a method are only persisted if no exception occurs
 */
@RestController
@RequestMapping("/api/store")
@Transactional
public class StoreController {

    private final OfferedItemRepository offeredItemRepository;
    private final BoughtItemResource boughtItemResource;
    private final UserService userService;
    private final OfferedItemResource offeredItemResource;
    private final BoughtItemRepository boughtItemRepository;

    public StoreController(
        OfferedItemRepository offeredItemRepository,
        BoughtItemResource boughtItemResource,
        UserService userService,
        OfferedItemResource offeredItemResource,
        BoughtItemRepository boughtItemRepository
    ) {
        this.offeredItemRepository = offeredItemRepository;
        this.boughtItemResource = boughtItemResource;
        this.userService = userService;
        this.offeredItemResource = offeredItemResource;
        this.boughtItemRepository = boughtItemRepository;
    }

    public record BuyRequest(@NotNull Long offeredItemId, @NotNull @Positive Integer amount) {}

    /**
     * Handles the complete purchase of multiple items at once including the reduction of the stock of every affected offered item.
     */
    @PostMapping("/buy-offered-items")
    public ResponseEntity<List<BoughtItem>> buyOfferedItems(@RequestBody List<@Valid @NotNull BuyRequest> buyRequests) {
        String offeredItemEntityName = "offeredItem";
        Consumer<List<BuyRequest>> validateNoEqualOfferIdsOrElseThrow = buyReqs ->
            buyReqs
                .stream()
                .collect(Collectors.groupingBy(BuyRequest::offeredItemId))
                .forEach((offerId, equalOfferId) -> {
                    if (equalOfferId.size() > 1) {
                        throw new BadRequestAlertException(
                            "Multiple buy requests with equal offer ID " + offerId,
                            offeredItemEntityName,
                            "sameOfferIds"
                        );
                    }
                });

        var buyer = userService.getUserWithAuthorities().orElseThrow();
        Function<BuyRequest, BoughtItem> newBoughtItem = buyRequest -> {
            var offer = offeredItemRepository.findById(buyRequest.offeredItemId()).orElseThrow();
            if (Boolean.TRUE.equals(offer.getHidden())) {
                throw new BadRequestAlertException("Can't buy hidden offer " + offer, offeredItemEntityName, "offerHidden");
            }
            return new BoughtItem(offer, buyRequest.amount(), buyer);
        };

        BiFunction<OfferedItem, Integer, OfferedItem> newReduceStockOfferUpdate = (OfferedItem offer, Integer reduceAmount) -> {
            int reducedStock = offer.getStock() - reduceAmount;
            if (reducedStock < 0) {
                throw new BadRequestAlertException(
                    "Following offer has not enough stock left: " + offer,
                    offeredItemEntityName,
                    "notEnoughStock"
                );
            }
            return new OfferedItem().id(offer.getId()).stock(reducedStock);
        };

        Consumer<OfferedItem> updateDatabaseOffer = Unchecked.consumer(offerUpdate -> {
            var updatedOfferResponse = offeredItemResource.partialUpdateOfferedItem(offerUpdate.getId(), offerUpdate);
            if (!updatedOfferResponse.getStatusCode().is2xxSuccessful()) {
                throw new BadRequestAlertException(
                    "Updating the offered item's stock failed with following response: " + updatedOfferResponse,
                    offeredItemEntityName,
                    "stockUpdateFailed"
                );
            }
        });

        Function<ResponseEntity<BoughtItem>, BoughtItem> getBodyOrElseThrow = creationResponse -> {
            if (creationResponse.getStatusCode().is2xxSuccessful()) {
                return Objects.requireNonNull(creationResponse.getBody());
            } else {
                throw new BadRequestAlertException(
                    "Creating the bought item stock failed with following response: " + creationResponse,
                    "boughtItem",
                    "boughtItemCreationFailed"
                );
            }
        };

        validateNoEqualOfferIdsOrElseThrow.accept(buyRequests);

        var boughtItems = buyRequests.stream().map(newBoughtItem).toList();
        boughtItems
            .stream()
            .map(boughtItem -> newReduceStockOfferUpdate.apply(boughtItem.getOffer(), boughtItem.getAmount()))
            .forEach(updateDatabaseOffer);
        return ResponseEntity.ok(
            boughtItems.stream().map(Unchecked.function(boughtItemResource::createBoughtItem)).map(getBodyOrElseThrow).toList()
        );
    }

    public record Invoice(Instant billingTime, List<BoughtItem> boughtItems) {}

    /**
     * Gets all invoices for a user. An invoice is just bought items grouped by their billing time.
     *
     * @see #billBoughtItems(BillingRequest)
     * @see #payBilledItems(PayRequest)
     */
    @GetMapping("/user-invoices/{userId}")
    public ResponseEntity<List<Invoice>> userInvoices(@PathVariable("userId") @Valid @NotNull Long userId) {
        var billed = boughtItemRepository.findBilledNotPaidByBuyerId(userId);
        var groupedByBillingTime = billed.stream().collect(Collectors.groupingBy(BoughtItem::getBillingTime));
        var invoices = groupedByBillingTime
            .entrySet()
            .stream()
            .map(billingTimeAndItems -> new Invoice(billingTimeAndItems.getKey(), billingTimeAndItems.getValue()))
            .sorted(Comparator.comparing(Invoice::billingTime))
            .toList();
        return ResponseEntity.ok(invoices);
    }

    public record BillingRequest(@NotNull Long userId, @NotNull Instant beforeIncluding) {}

    /**
     * Sets all bought items (before a specified time) of a user to billed.
     * This means that an admin requests a user to pay his items.
     */
    @PostMapping("/billing")
    public ResponseEntity<List<BoughtItem>> billBoughtItems(@RequestBody @Valid @NotNull StoreController.BillingRequest billingRequest) {
        return forEachSetAndSave(
            boughtItemRepository.findUnbilledBeforeIncluding(billingRequest.userId, billingRequest.beforeIncluding),
            BoughtItem::setBillingTime
        );
    }

    public record PayRequest(@NotNull Long userId, @NotNull Instant billingTime) {}

    /**
     * Sets a bill (specified by its billing time and user) to paid by setting every item in that bill to paid.
     * This means that an admin has confirmed that a user has paid that bill.
     *
     * @see #userInvoices(Long)
     */
    @PostMapping("/pay")
    public ResponseEntity<List<BoughtItem>> payBilledItems(@RequestBody @Valid @NotNull StoreController.PayRequest payRequest) {
        return forEachSetAndSave(
            boughtItemRepository.findBilledNotPaidByBuyerIdAndBillingTime(payRequest.userId, payRequest.billingTime),
            BoughtItem::setPaidTime
        );
    }

    private ResponseEntity<List<BoughtItem>> forEachSetAndSave(List<BoughtItem> items, BiConsumer<BoughtItem, Instant> setter) {
        if (items.isEmpty()) {
            return ResponseEntity.ok(List.of());
        }
        var modificationTimestamp = Instant.now();
        items.forEach(item -> setter.accept(item, modificationTimestamp));
        return ResponseEntity.ok(boughtItemRepository.saveAll(items));
    }

    public record PayItemsRequest(@NotNull @NotEmpty List<BoughtItem> items) {}

    /**
     * Sets all given bought items to paid
     *
     * @see #userInvoices(Long)
     */
    @PostMapping("/pay-items")
    public ResponseEntity<List<BoughtItem>> payItems(@RequestBody @Valid @NotNull PayItemsRequest payItemsRequest) {
        var ids = payItemsRequest.items().stream().map(BoughtItem::getId).toList();
        return forEachSetAndSave(boughtItemRepository.findAllById(ids), BoughtItem::setPaidTime);
    }
}
