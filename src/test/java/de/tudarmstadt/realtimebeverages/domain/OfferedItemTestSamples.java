package de.tudarmstadt.realtimebeverages.domain;

import java.util.Random;
import java.util.UUID;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;

public class OfferedItemTestSamples {

    private static final Random random = new Random();
    private static final AtomicLong longCount = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));
    private static final AtomicInteger intCount = new AtomicInteger(random.nextInt() + (2 * Short.MAX_VALUE));

    public static OfferedItem getOfferedItemSample1() {
        return new OfferedItem().id(1L).name("name1").stock(1).limitedStockThreshold(1).category("category1");
    }

    public static OfferedItem getOfferedItemSample2() {
        return new OfferedItem().id(2L).name("name2").stock(2).limitedStockThreshold(2).category("category2");
    }

    public static OfferedItem getOfferedItemRandomSampleGenerator() {
        return new OfferedItem()
            .id(longCount.incrementAndGet())
            .name(UUID.randomUUID().toString())
            .stock(intCount.incrementAndGet())
            .limitedStockThreshold(intCount.incrementAndGet())
            .category(UUID.randomUUID().toString());
    }
}
