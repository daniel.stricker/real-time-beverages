package de.tudarmstadt.realtimebeverages.domain;

import static de.tudarmstadt.realtimebeverages.domain.BoughtItemTestSamples.*;
import static de.tudarmstadt.realtimebeverages.domain.OfferedItemTestSamples.*;
import static org.assertj.core.api.Assertions.assertThat;

import de.tudarmstadt.realtimebeverages.web.rest.TestUtil;
import org.junit.jupiter.api.Test;

class BoughtItemTest {

    @Test
    void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(BoughtItem.class);
        BoughtItem boughtItem1 = getBoughtItemSample1();
        BoughtItem boughtItem2 = new BoughtItem();
        assertThat(boughtItem1).isNotEqualTo(boughtItem2);

        boughtItem2.setId(boughtItem1.getId());
        assertThat(boughtItem1).isEqualTo(boughtItem2);

        boughtItem2 = getBoughtItemSample2();
        assertThat(boughtItem1).isNotEqualTo(boughtItem2);
    }

    @Test
    void offerTest() {
        BoughtItem boughtItem = getBoughtItemRandomSampleGenerator();
        OfferedItem offeredItemBack = getOfferedItemRandomSampleGenerator();

        boughtItem.setOffer(offeredItemBack);
        assertThat(boughtItem.getOffer()).isEqualTo(offeredItemBack);

        boughtItem.offer(null);
        assertThat(boughtItem.getOffer()).isNull();
    }
}
