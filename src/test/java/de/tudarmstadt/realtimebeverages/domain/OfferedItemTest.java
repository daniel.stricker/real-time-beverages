package de.tudarmstadt.realtimebeverages.domain;

import static de.tudarmstadt.realtimebeverages.domain.OfferedItemTestSamples.*;
import static org.assertj.core.api.Assertions.assertThat;

import de.tudarmstadt.realtimebeverages.web.rest.TestUtil;
import org.junit.jupiter.api.Test;

class OfferedItemTest {

    @Test
    void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(OfferedItem.class);
        OfferedItem offeredItem1 = getOfferedItemSample1();
        OfferedItem offeredItem2 = new OfferedItem();
        assertThat(offeredItem1).isNotEqualTo(offeredItem2);

        offeredItem2.setId(offeredItem1.getId());
        assertThat(offeredItem1).isEqualTo(offeredItem2);

        offeredItem2 = getOfferedItemSample2();
        assertThat(offeredItem1).isNotEqualTo(offeredItem2);
    }
}
