package de.tudarmstadt.realtimebeverages.domain;

import java.util.Random;
import java.util.UUID;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;

public class BoughtItemTestSamples {

    private static final Random random = new Random();
    private static final AtomicLong longCount = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));
    private static final AtomicInteger intCount = new AtomicInteger(random.nextInt() + (2 * Short.MAX_VALUE));

    public static BoughtItem getBoughtItemSample1() {
        return new BoughtItem().id(1L).name("name1").amount(1);
    }

    public static BoughtItem getBoughtItemSample2() {
        return new BoughtItem().id(2L).name("name2").amount(2);
    }

    public static BoughtItem getBoughtItemRandomSampleGenerator() {
        return new BoughtItem().id(longCount.incrementAndGet()).name(UUID.randomUUID().toString()).amount(intCount.incrementAndGet());
    }
}
