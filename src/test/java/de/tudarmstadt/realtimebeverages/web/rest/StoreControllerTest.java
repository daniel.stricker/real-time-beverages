package de.tudarmstadt.realtimebeverages.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatExceptionOfType;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.when;

import de.tudarmstadt.realtimebeverages.domain.BoughtItem;
import de.tudarmstadt.realtimebeverages.domain.OfferedItem;
import de.tudarmstadt.realtimebeverages.domain.User;
import de.tudarmstadt.realtimebeverages.repository.BoughtItemRepository;
import de.tudarmstadt.realtimebeverages.repository.OfferedItemRepository;
import de.tudarmstadt.realtimebeverages.service.UserService;
import de.tudarmstadt.realtimebeverages.web.rest.errors.BadRequestAlertException;
import java.net.URISyntaxException;
import java.time.Instant;
import java.util.List;
import java.util.Optional;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.ResponseEntity;

class StoreControllerTest {

    private StoreController storeController;

    @Mock
    private OfferedItemRepository offeredItemRepository;

    @Mock
    private BoughtItemResource boughtItemResource;

    @Mock
    private UserService userService;

    @Mock
    private OfferedItemResource offeredItemResource;

    @Mock
    private BoughtItemRepository boughtItemRepository;

    private AutoCloseable annotationMocks;

    @BeforeEach
    void setUp() {
        annotationMocks = MockitoAnnotations.openMocks(this);

        storeController = new StoreController(
            offeredItemRepository,
            boughtItemResource,
            userService,
            offeredItemResource,
            boughtItemRepository
        );
    }

    @AfterEach
    void tearDown() throws Exception {
        annotationMocks.close();
    }

    @Test
    void buyOfferedItems() throws URISyntaxException {
        var buyer = new User();
        buyer.setId(1L);
        when(userService.getUserWithAuthorities()).thenReturn(Optional.of(buyer));

        var buyRequestsEqualIds = List.of(new StoreController.BuyRequest(1L, 2), new StoreController.BuyRequest(1L, 3));
        assertThatExceptionOfType(BadRequestAlertException.class).isThrownBy(() -> storeController.buyOfferedItems(buyRequestsEqualIds));

        var buyRequestsHidden = List.of(new StoreController.BuyRequest(2L, 2));
        var hiddenOffer = new OfferedItem();
        hiddenOffer.setHidden(true);
        when(offeredItemRepository.findById(2L)).thenReturn(Optional.of(hiddenOffer));
        assertThatExceptionOfType(BadRequestAlertException.class).isThrownBy(() -> storeController.buyOfferedItems(buyRequestsHidden));

        var buyRequestsNotEnoughStock = List.of(new StoreController.BuyRequest(3L, 2));
        var offerNotEnoughStock = new OfferedItem();
        offerNotEnoughStock.setHidden(false);
        offerNotEnoughStock.setStock(1);
        when(offeredItemRepository.findById(3L)).thenReturn(Optional.of(offerNotEnoughStock));
        assertThatExceptionOfType(BadRequestAlertException.class).isThrownBy(() ->
            storeController.buyOfferedItems(buyRequestsNotEnoughStock)
        );

        var buyRequestsUpdateFail = List.of(new StoreController.BuyRequest(4L, 2));
        var offerUpdateFail = new OfferedItem();
        offerUpdateFail.setId(4L);
        offerUpdateFail.setHidden(false);
        offerUpdateFail.setStock(3);
        when(offeredItemRepository.findById(4L)).thenReturn(Optional.of(offerUpdateFail));
        when(offeredItemResource.partialUpdateOfferedItem(eq(4L), any())).thenReturn(ResponseEntity.badRequest().body(null));
        assertThatExceptionOfType(BadRequestAlertException.class).isThrownBy(() -> storeController.buyOfferedItems(buyRequestsUpdateFail));

        var buyRequestsSuccess = List.of(new StoreController.BuyRequest(5L, 2));
        var offer = new OfferedItem();
        offer.setId(5L);
        offer.setHidden(false);
        offer.setStock(3);
        when(offeredItemRepository.findById(5L)).thenReturn(Optional.of(offer));
        when(offeredItemResource.partialUpdateOfferedItem(eq(5L), any())).thenReturn(ResponseEntity.ok(null));
        var boughtItem = new BoughtItem(offer, 2, buyer);
        when(boughtItemResource.createBoughtItem(any())).thenReturn(ResponseEntity.ok(boughtItem));
        assertThat(storeController.buyOfferedItems(buyRequestsSuccess)).isEqualTo(ResponseEntity.ok(List.of(boughtItem)));
    }

    @Test
    void billBoughtItems() {
        var billingRequest = new StoreController.BillingRequest(1L, Instant.now());
        var item1 = new BoughtItem(0L, null, null, null, null, null, null, null, null);
        var item2 = new BoughtItem(1L, null, null, null, null, null, null, null, null);
        var item3 = new BoughtItem(2L, null, null, null, null, null, null, null, null);
        var items = List.of(item1, item2, item3);
        when(boughtItemRepository.findUnbilledBeforeIncluding(billingRequest.userId(), billingRequest.beforeIncluding())).thenReturn(items);
        when(boughtItemRepository.saveAll(items)).thenReturn(items);
        assertThat(storeController.billBoughtItems(billingRequest).getBody()).isSameAs(items);
        assertThat(items).map(BoughtItem::getBillingTime).doesNotContainNull();

        when(boughtItemRepository.findBilledNotPaidByBuyerIdAndBillingTime(2L, billingRequest.beforeIncluding())).thenReturn(List.of());
        var badArg = new StoreController.BillingRequest(2L, billingRequest.beforeIncluding());
        assertThat(storeController.billBoughtItems(badArg).getBody()).isEmpty();
    }

    @Test
    void payBilledItems() {
        var payRequest = new StoreController.PayRequest(1L, Instant.now());
        var someBillingTime = Instant.now();
        var anotherBillingTime = Instant.now().minusSeconds(1);
        var item1 = new BoughtItem(0L, null, null, null, someBillingTime, null, null, null, null);
        var item2 = new BoughtItem(1L, null, null, null, someBillingTime, null, null, null, null);
        var item3 = new BoughtItem(2L, null, null, null, anotherBillingTime, null, null, null, null);
        var items = List.of(item1, item2, item3);
        when(boughtItemRepository.findBilledNotPaidByBuyerIdAndBillingTime(payRequest.userId(), payRequest.billingTime())).thenReturn(
            items
        );
        when(boughtItemRepository.saveAll(items)).thenReturn(items);
        assertThat(storeController.payBilledItems(payRequest).getBody()).isSameAs(items);
        assertThat(items).map(BoughtItem::getPaidTime).doesNotContainNull();

        when(boughtItemRepository.findBilledNotPaidByBuyerIdAndBillingTime(2L, payRequest.billingTime())).thenReturn(List.of());
        var badArg = new StoreController.PayRequest(2L, payRequest.billingTime());
        assertThat(storeController.payBilledItems(badArg).getBody()).isEmpty();
    }

    @Test
    void payItems() {
        var item1 = new BoughtItem(0L, null, null, null, null, null, null, null, null);
        var item2 = new BoughtItem(1L, null, null, null, null, null, null, null, null);
        var item3 = new BoughtItem(2L, null, null, null, null, null, null, null, null);
        var items = List.of(item1, item2, item3);
        var payRequest = new StoreController.PayItemsRequest(items);

        when(boughtItemRepository.findAllById(items.stream().map(BoughtItem::getId).toList())).thenReturn(items);
        when(boughtItemRepository.saveAll(items)).thenReturn(items);
        var result = storeController.payItems(payRequest).getBody();
        assertThat(result).map(BoughtItem::getPaidTime).doesNotContainNull();
    }

    @Test
    void userInvoices() {
        var laterBilling = Instant.now();
        var earlierBilling = Instant.now().minusSeconds(1);
        var item1 = new BoughtItem(0L, null, null, null, laterBilling, null, null, null, null);
        var item2 = new BoughtItem(1L, null, null, null, laterBilling, null, null, null, null);
        var item3 = new BoughtItem(2L, null, null, null, earlierBilling, null, null, null, null);
        when(boughtItemRepository.findBilledNotPaidByBuyerId(1L)).thenReturn(List.of(item1, item2, item3));
        var expectedInvoicesSortedByBillingTime = List.of(
            new StoreController.Invoice(earlierBilling, List.of(item3)),
            new StoreController.Invoice(laterBilling, List.of(item1, item2))
        );
        assertThat(storeController.userInvoices(1L)).isEqualTo(ResponseEntity.ok(expectedInvoicesSortedByBillingTime));

        when(boughtItemRepository.findBilledNotPaidByBuyerId(2L)).thenReturn(List.of());
        assertThat(storeController.userInvoices(2L)).isEqualTo(ResponseEntity.ok(List.of()));
    }
}
