package de.tudarmstadt.realtimebeverages.web.rest;

import static de.tudarmstadt.realtimebeverages.domain.BoughtItemAsserts.*;
import static de.tudarmstadt.realtimebeverages.web.rest.TestUtil.createUpdateProxyForBean;
import static de.tudarmstadt.realtimebeverages.web.rest.TestUtil.sameNumber;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import com.fasterxml.jackson.databind.ObjectMapper;
import de.tudarmstadt.realtimebeverages.IntegrationTest;
import de.tudarmstadt.realtimebeverages.domain.BoughtItem;
import de.tudarmstadt.realtimebeverages.domain.OfferedItem;
import de.tudarmstadt.realtimebeverages.domain.User;
import de.tudarmstadt.realtimebeverages.repository.BoughtItemRepository;
import de.tudarmstadt.realtimebeverages.repository.UserRepository;
import jakarta.persistence.EntityManager;
import java.math.BigDecimal;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

/**
 * Integration tests for the {@link BoughtItemResource} REST controller.
 */
@IntegrationTest
@AutoConfigureMockMvc
@WithMockUser
class BoughtItemResourceIT {

    private static final String DEFAULT_NAME = "AAAAAAAAAA";
    private static final String UPDATED_NAME = "BBBBBBBBBB";

    private static final BigDecimal DEFAULT_PRICE = new BigDecimal(0);
    private static final BigDecimal UPDATED_PRICE = new BigDecimal(1);

    private static final Instant DEFAULT_BOUGHT_TIME = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_BOUGHT_TIME = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final Instant DEFAULT_BILLING_TIME = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_BILLING_TIME = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final Instant DEFAULT_PAID_TIME = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_PAID_TIME = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final Integer DEFAULT_AMOUNT = 1;
    private static final Integer UPDATED_AMOUNT = 2;

    private static final String ENTITY_API_URL = "/api/bought-items";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";

    private static Random random = new Random();
    private static AtomicLong longCount = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    @Autowired
    private ObjectMapper om;

    @Autowired
    private BoughtItemRepository boughtItemRepository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restBoughtItemMockMvc;

    private BoughtItem boughtItem;

    private BoughtItem insertedBoughtItem;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static BoughtItem createEntity(EntityManager em) {
        BoughtItem boughtItem = new BoughtItem()
            .name(DEFAULT_NAME)
            .price(DEFAULT_PRICE)
            .boughtTime(DEFAULT_BOUGHT_TIME)
            .billingTime(DEFAULT_BILLING_TIME)
            .paidTime(DEFAULT_PAID_TIME)
            .amount(DEFAULT_AMOUNT);
        // Add required entity
        OfferedItem offeredItem;
        if (TestUtil.findAll(em, OfferedItem.class).isEmpty()) {
            offeredItem = OfferedItemResourceIT.createEntity(em);
            em.persist(offeredItem);
            em.flush();
        } else {
            offeredItem = TestUtil.findAll(em, OfferedItem.class).get(0);
        }
        boughtItem.setOffer(offeredItem);
        // Add required entity
        User user = UserResourceIT.createEntity(em);
        em.persist(user);
        em.flush();
        boughtItem.setBuyer(user);
        return boughtItem;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static BoughtItem createUpdatedEntity(EntityManager em) {
        BoughtItem boughtItem = new BoughtItem()
            .name(UPDATED_NAME)
            .price(UPDATED_PRICE)
            .boughtTime(UPDATED_BOUGHT_TIME)
            .billingTime(UPDATED_BILLING_TIME)
            .paidTime(UPDATED_PAID_TIME)
            .amount(UPDATED_AMOUNT);
        // Add required entity
        OfferedItem offeredItem;
        if (TestUtil.findAll(em, OfferedItem.class).isEmpty()) {
            offeredItem = OfferedItemResourceIT.createUpdatedEntity(em);
            em.persist(offeredItem);
            em.flush();
        } else {
            offeredItem = TestUtil.findAll(em, OfferedItem.class).get(0);
        }
        boughtItem.setOffer(offeredItem);
        // Add required entity
        User user = UserResourceIT.createEntity(em);
        em.persist(user);
        em.flush();
        boughtItem.setBuyer(user);
        return boughtItem;
    }

    @BeforeEach
    public void initTest() {
        boughtItem = createEntity(em);
    }

    @AfterEach
    public void cleanup() {
        if (insertedBoughtItem != null) {
            boughtItemRepository.delete(insertedBoughtItem);
            insertedBoughtItem = null;
        }
    }

    @Test
    @Transactional
    void createBoughtItem() throws Exception {
        long databaseSizeBeforeCreate = getRepositoryCount();
        // Create the BoughtItem
        var returnedBoughtItem = om.readValue(
            restBoughtItemMockMvc
                .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(om.writeValueAsBytes(boughtItem)))
                .andExpect(status().isCreated())
                .andReturn()
                .getResponse()
                .getContentAsString(),
            BoughtItem.class
        );

        // Validate the BoughtItem in the database
        assertIncrementedRepositoryCount(databaseSizeBeforeCreate);
        assertBoughtItemUpdatableFieldsEquals(returnedBoughtItem, getPersistedBoughtItem(returnedBoughtItem));

        insertedBoughtItem = returnedBoughtItem;
    }

    @Test
    @Transactional
    void createBoughtItemWithExistingId() throws Exception {
        // Create the BoughtItem with an existing ID
        boughtItem.setId(1L);

        long databaseSizeBeforeCreate = getRepositoryCount();

        // An entity with an existing ID cannot be created, so this API call must fail
        restBoughtItemMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(om.writeValueAsBytes(boughtItem)))
            .andExpect(status().isBadRequest());

        // Validate the BoughtItem in the database
        assertSameRepositoryCount(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    void checkNameIsRequired() throws Exception {
        long databaseSizeBeforeTest = getRepositoryCount();
        // set the field null
        boughtItem.setName(null);

        // Create the BoughtItem, which fails.

        restBoughtItemMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(om.writeValueAsBytes(boughtItem)))
            .andExpect(status().isBadRequest());

        assertSameRepositoryCount(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void checkPriceIsRequired() throws Exception {
        long databaseSizeBeforeTest = getRepositoryCount();
        // set the field null
        boughtItem.setPrice(null);

        // Create the BoughtItem, which fails.

        restBoughtItemMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(om.writeValueAsBytes(boughtItem)))
            .andExpect(status().isBadRequest());

        assertSameRepositoryCount(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void checkBoughtTimeIsRequired() throws Exception {
        long databaseSizeBeforeTest = getRepositoryCount();
        // set the field null
        boughtItem.setBoughtTime(null);

        // Create the BoughtItem, which fails.

        restBoughtItemMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(om.writeValueAsBytes(boughtItem)))
            .andExpect(status().isBadRequest());

        assertSameRepositoryCount(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void checkAmountIsRequired() throws Exception {
        long databaseSizeBeforeTest = getRepositoryCount();
        // set the field null
        boughtItem.setAmount(null);

        // Create the BoughtItem, which fails.

        restBoughtItemMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(om.writeValueAsBytes(boughtItem)))
            .andExpect(status().isBadRequest());

        assertSameRepositoryCount(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void getAllBoughtItems() throws Exception {
        // Initialize the database
        insertedBoughtItem = boughtItemRepository.saveAndFlush(boughtItem);

        // Get all the boughtItemList
        restBoughtItemMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(boughtItem.getId().intValue())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME)))
            .andExpect(jsonPath("$.[*].price").value(hasItem(sameNumber(DEFAULT_PRICE))))
            .andExpect(jsonPath("$.[*].boughtTime").value(hasItem(DEFAULT_BOUGHT_TIME.toString())))
            .andExpect(jsonPath("$.[*].billingTime").value(hasItem(DEFAULT_BILLING_TIME.toString())))
            .andExpect(jsonPath("$.[*].paidTime").value(hasItem(DEFAULT_PAID_TIME.toString())))
            .andExpect(jsonPath("$.[*].amount").value(hasItem(DEFAULT_AMOUNT)));
    }

    @Test
    @Transactional
    void getBoughtItem() throws Exception {
        // Initialize the database
        insertedBoughtItem = boughtItemRepository.saveAndFlush(boughtItem);

        // Get the boughtItem
        restBoughtItemMockMvc
            .perform(get(ENTITY_API_URL_ID, boughtItem.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(boughtItem.getId().intValue()))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME))
            .andExpect(jsonPath("$.price").value(sameNumber(DEFAULT_PRICE)))
            .andExpect(jsonPath("$.boughtTime").value(DEFAULT_BOUGHT_TIME.toString()))
            .andExpect(jsonPath("$.billingTime").value(DEFAULT_BILLING_TIME.toString()))
            .andExpect(jsonPath("$.paidTime").value(DEFAULT_PAID_TIME.toString()))
            .andExpect(jsonPath("$.amount").value(DEFAULT_AMOUNT));
    }

    @Test
    @Transactional
    void getNonExistingBoughtItem() throws Exception {
        // Get the boughtItem
        restBoughtItemMockMvc.perform(get(ENTITY_API_URL_ID, Long.MAX_VALUE)).andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    void putExistingBoughtItem() throws Exception {
        // Initialize the database
        insertedBoughtItem = boughtItemRepository.saveAndFlush(boughtItem);

        long databaseSizeBeforeUpdate = getRepositoryCount();

        // Update the boughtItem
        BoughtItem updatedBoughtItem = boughtItemRepository.findById(boughtItem.getId()).orElseThrow();
        // Disconnect from session so that the updates on updatedBoughtItem are not directly saved in db
        em.detach(updatedBoughtItem);
        updatedBoughtItem
            .name(UPDATED_NAME)
            .price(UPDATED_PRICE)
            .boughtTime(UPDATED_BOUGHT_TIME)
            .billingTime(UPDATED_BILLING_TIME)
            .paidTime(UPDATED_PAID_TIME)
            .amount(UPDATED_AMOUNT);

        restBoughtItemMockMvc
            .perform(
                put(ENTITY_API_URL_ID, updatedBoughtItem.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(om.writeValueAsBytes(updatedBoughtItem))
            )
            .andExpect(status().isOk());

        // Validate the BoughtItem in the database
        assertSameRepositoryCount(databaseSizeBeforeUpdate);
        assertPersistedBoughtItemToMatchAllProperties(updatedBoughtItem);
    }

    @Test
    @Transactional
    void putNonExistingBoughtItem() throws Exception {
        long databaseSizeBeforeUpdate = getRepositoryCount();
        boughtItem.setId(longCount.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restBoughtItemMockMvc
            .perform(
                put(ENTITY_API_URL_ID, boughtItem.getId()).contentType(MediaType.APPLICATION_JSON).content(om.writeValueAsBytes(boughtItem))
            )
            .andExpect(status().isBadRequest());

        // Validate the BoughtItem in the database
        assertSameRepositoryCount(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithIdMismatchBoughtItem() throws Exception {
        long databaseSizeBeforeUpdate = getRepositoryCount();
        boughtItem.setId(longCount.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restBoughtItemMockMvc
            .perform(
                put(ENTITY_API_URL_ID, longCount.incrementAndGet())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(om.writeValueAsBytes(boughtItem))
            )
            .andExpect(status().isBadRequest());

        // Validate the BoughtItem in the database
        assertSameRepositoryCount(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithMissingIdPathParamBoughtItem() throws Exception {
        long databaseSizeBeforeUpdate = getRepositoryCount();
        boughtItem.setId(longCount.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restBoughtItemMockMvc
            .perform(put(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(om.writeValueAsBytes(boughtItem)))
            .andExpect(status().isMethodNotAllowed());

        // Validate the BoughtItem in the database
        assertSameRepositoryCount(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void partialUpdateBoughtItemWithPatch() throws Exception {
        // Initialize the database
        insertedBoughtItem = boughtItemRepository.saveAndFlush(boughtItem);

        long databaseSizeBeforeUpdate = getRepositoryCount();

        // Update the boughtItem using partial update
        BoughtItem partialUpdatedBoughtItem = new BoughtItem();
        partialUpdatedBoughtItem.setId(boughtItem.getId());

        partialUpdatedBoughtItem.paidTime(UPDATED_PAID_TIME);

        restBoughtItemMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedBoughtItem.getId())
                    .contentType("application/merge-patch+json")
                    .content(om.writeValueAsBytes(partialUpdatedBoughtItem))
            )
            .andExpect(status().isOk());

        // Validate the BoughtItem in the database

        assertSameRepositoryCount(databaseSizeBeforeUpdate);
        assertBoughtItemUpdatableFieldsEquals(
            createUpdateProxyForBean(partialUpdatedBoughtItem, boughtItem),
            getPersistedBoughtItem(boughtItem)
        );
    }

    @Test
    @Transactional
    void fullUpdateBoughtItemWithPatch() throws Exception {
        // Initialize the database
        insertedBoughtItem = boughtItemRepository.saveAndFlush(boughtItem);

        long databaseSizeBeforeUpdate = getRepositoryCount();

        // Update the boughtItem using partial update
        BoughtItem partialUpdatedBoughtItem = new BoughtItem();
        partialUpdatedBoughtItem.setId(boughtItem.getId());

        partialUpdatedBoughtItem
            .name(UPDATED_NAME)
            .price(UPDATED_PRICE)
            .boughtTime(UPDATED_BOUGHT_TIME)
            .billingTime(UPDATED_BILLING_TIME)
            .paidTime(UPDATED_PAID_TIME)
            .amount(UPDATED_AMOUNT);

        restBoughtItemMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedBoughtItem.getId())
                    .contentType("application/merge-patch+json")
                    .content(om.writeValueAsBytes(partialUpdatedBoughtItem))
            )
            .andExpect(status().isOk());

        // Validate the BoughtItem in the database

        assertSameRepositoryCount(databaseSizeBeforeUpdate);
        assertBoughtItemUpdatableFieldsEquals(partialUpdatedBoughtItem, getPersistedBoughtItem(partialUpdatedBoughtItem));
    }

    @Test
    @Transactional
    void patchNonExistingBoughtItem() throws Exception {
        long databaseSizeBeforeUpdate = getRepositoryCount();
        boughtItem.setId(longCount.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restBoughtItemMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, boughtItem.getId())
                    .contentType("application/merge-patch+json")
                    .content(om.writeValueAsBytes(boughtItem))
            )
            .andExpect(status().isBadRequest());

        // Validate the BoughtItem in the database
        assertSameRepositoryCount(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithIdMismatchBoughtItem() throws Exception {
        long databaseSizeBeforeUpdate = getRepositoryCount();
        boughtItem.setId(longCount.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restBoughtItemMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, longCount.incrementAndGet())
                    .contentType("application/merge-patch+json")
                    .content(om.writeValueAsBytes(boughtItem))
            )
            .andExpect(status().isBadRequest());

        // Validate the BoughtItem in the database
        assertSameRepositoryCount(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithMissingIdPathParamBoughtItem() throws Exception {
        long databaseSizeBeforeUpdate = getRepositoryCount();
        boughtItem.setId(longCount.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restBoughtItemMockMvc
            .perform(patch(ENTITY_API_URL).contentType("application/merge-patch+json").content(om.writeValueAsBytes(boughtItem)))
            .andExpect(status().isMethodNotAllowed());

        // Validate the BoughtItem in the database
        assertSameRepositoryCount(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void deleteBoughtItem() throws Exception {
        // Initialize the database
        insertedBoughtItem = boughtItemRepository.saveAndFlush(boughtItem);

        long databaseSizeBeforeDelete = getRepositoryCount();

        // Delete the boughtItem
        restBoughtItemMockMvc
            .perform(delete(ENTITY_API_URL_ID, boughtItem.getId()).accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        assertDecrementedRepositoryCount(databaseSizeBeforeDelete);
    }

    protected long getRepositoryCount() {
        return boughtItemRepository.count();
    }

    protected void assertIncrementedRepositoryCount(long countBefore) {
        assertThat(countBefore + 1).isEqualTo(getRepositoryCount());
    }

    protected void assertDecrementedRepositoryCount(long countBefore) {
        assertThat(countBefore - 1).isEqualTo(getRepositoryCount());
    }

    protected void assertSameRepositoryCount(long countBefore) {
        assertThat(countBefore).isEqualTo(getRepositoryCount());
    }

    protected BoughtItem getPersistedBoughtItem(BoughtItem boughtItem) {
        return boughtItemRepository.findById(boughtItem.getId()).orElseThrow();
    }

    protected void assertPersistedBoughtItemToMatchAllProperties(BoughtItem expectedBoughtItem) {
        assertBoughtItemAllPropertiesEquals(expectedBoughtItem, getPersistedBoughtItem(expectedBoughtItem));
    }

    protected void assertPersistedBoughtItemToMatchUpdatableProperties(BoughtItem expectedBoughtItem) {
        assertBoughtItemAllUpdatablePropertiesEquals(expectedBoughtItem, getPersistedBoughtItem(expectedBoughtItem));
    }
}
