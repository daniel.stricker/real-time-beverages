package de.tudarmstadt.realtimebeverages.web.rest;

import static de.tudarmstadt.realtimebeverages.domain.OfferedItemAsserts.*;
import static de.tudarmstadt.realtimebeverages.web.rest.TestUtil.createUpdateProxyForBean;
import static de.tudarmstadt.realtimebeverages.web.rest.TestUtil.sameNumber;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import com.fasterxml.jackson.databind.ObjectMapper;
import de.tudarmstadt.realtimebeverages.IntegrationTest;
import de.tudarmstadt.realtimebeverages.domain.OfferedItem;
import de.tudarmstadt.realtimebeverages.repository.OfferedItemRepository;
import jakarta.persistence.EntityManager;
import java.math.BigDecimal;
import java.util.Base64;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

/**
 * Integration tests for the {@link OfferedItemResource} REST controller.
 */
@IntegrationTest
@AutoConfigureMockMvc
@WithMockUser
class OfferedItemResourceIT {

    private static final String DEFAULT_NAME = "AAAAAAAAAA";
    private static final String UPDATED_NAME = "BBBBBBBBBB";

    private static final Integer DEFAULT_STOCK = 0;
    private static final Integer UPDATED_STOCK = 1;

    private static final BigDecimal DEFAULT_PRICE = new BigDecimal(0);
    private static final BigDecimal UPDATED_PRICE = new BigDecimal(1);

    private static final Boolean DEFAULT_HIDDEN = false;
    private static final Boolean UPDATED_HIDDEN = true;

    private static final byte[] DEFAULT_IMAGE = TestUtil.createByteArray(1, "0");
    private static final byte[] UPDATED_IMAGE = TestUtil.createByteArray(1, "1");
    private static final String DEFAULT_IMAGE_CONTENT_TYPE = "image/jpg";
    private static final String UPDATED_IMAGE_CONTENT_TYPE = "image/png";

    private static final Integer DEFAULT_LIMITED_STOCK_THRESHOLD = 0;
    private static final Integer UPDATED_LIMITED_STOCK_THRESHOLD = 1;

    private static final String DEFAULT_CATEGORY = "AAAAAAAAAA";
    private static final String UPDATED_CATEGORY = "BBBBBBBBBB";

    private static final String ENTITY_API_URL = "/api/offered-items";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";

    private static Random random = new Random();
    private static AtomicLong longCount = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    @Autowired
    private ObjectMapper om;

    @Autowired
    private OfferedItemRepository offeredItemRepository;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restOfferedItemMockMvc;

    private OfferedItem offeredItem;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static OfferedItem createEntity(EntityManager em) {
        OfferedItem offeredItem = new OfferedItem()
            .name(DEFAULT_NAME)
            .stock(DEFAULT_STOCK)
            .price(DEFAULT_PRICE)
            .hidden(DEFAULT_HIDDEN)
            .image(DEFAULT_IMAGE)
            .imageContentType(DEFAULT_IMAGE_CONTENT_TYPE)
            .limitedStockThreshold(DEFAULT_LIMITED_STOCK_THRESHOLD)
            .category(DEFAULT_CATEGORY);
        return offeredItem;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static OfferedItem createUpdatedEntity(EntityManager em) {
        OfferedItem offeredItem = new OfferedItem()
            .name(UPDATED_NAME)
            .stock(UPDATED_STOCK)
            .price(UPDATED_PRICE)
            .hidden(UPDATED_HIDDEN)
            .image(UPDATED_IMAGE)
            .imageContentType(UPDATED_IMAGE_CONTENT_TYPE)
            .limitedStockThreshold(UPDATED_LIMITED_STOCK_THRESHOLD)
            .category(UPDATED_CATEGORY);
        return offeredItem;
    }

    @BeforeEach
    public void initTest() {
        offeredItem = createEntity(em);
    }

    @Test
    @Transactional
    void createOfferedItem() throws Exception {
        long databaseSizeBeforeCreate = getRepositoryCount();
        // Create the OfferedItem
        var returnedOfferedItem = om.readValue(
            restOfferedItemMockMvc
                .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(om.writeValueAsBytes(offeredItem)))
                .andExpect(status().isCreated())
                .andReturn()
                .getResponse()
                .getContentAsString(),
            OfferedItem.class
        );

        // Validate the OfferedItem in the database
        assertIncrementedRepositoryCount(databaseSizeBeforeCreate);
        assertOfferedItemUpdatableFieldsEquals(returnedOfferedItem, getPersistedOfferedItem(returnedOfferedItem));
    }

    @Test
    @Transactional
    void createOfferedItemWithExistingId() throws Exception {
        // Create the OfferedItem with an existing ID
        offeredItem.setId(1L);

        long databaseSizeBeforeCreate = getRepositoryCount();

        // An entity with an existing ID cannot be created, so this API call must fail
        restOfferedItemMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(om.writeValueAsBytes(offeredItem)))
            .andExpect(status().isBadRequest());

        // Validate the OfferedItem in the database
        assertSameRepositoryCount(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    void checkNameIsRequired() throws Exception {
        long databaseSizeBeforeTest = getRepositoryCount();
        // set the field null
        offeredItem.setName(null);

        // Create the OfferedItem, which fails.

        restOfferedItemMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(om.writeValueAsBytes(offeredItem)))
            .andExpect(status().isBadRequest());

        assertSameRepositoryCount(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void checkStockIsRequired() throws Exception {
        long databaseSizeBeforeTest = getRepositoryCount();
        // set the field null
        offeredItem.setStock(null);

        // Create the OfferedItem, which fails.

        restOfferedItemMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(om.writeValueAsBytes(offeredItem)))
            .andExpect(status().isBadRequest());

        assertSameRepositoryCount(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void checkPriceIsRequired() throws Exception {
        long databaseSizeBeforeTest = getRepositoryCount();
        // set the field null
        offeredItem.setPrice(null);

        // Create the OfferedItem, which fails.

        restOfferedItemMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(om.writeValueAsBytes(offeredItem)))
            .andExpect(status().isBadRequest());

        assertSameRepositoryCount(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void checkHiddenIsRequired() throws Exception {
        long databaseSizeBeforeTest = getRepositoryCount();
        // set the field null
        offeredItem.setHidden(null);

        // Create the OfferedItem, which fails.

        restOfferedItemMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(om.writeValueAsBytes(offeredItem)))
            .andExpect(status().isBadRequest());

        assertSameRepositoryCount(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void checkLimitedStockThresholdIsRequired() throws Exception {
        long databaseSizeBeforeTest = getRepositoryCount();
        // set the field null
        offeredItem.setLimitedStockThreshold(null);

        // Create the OfferedItem, which fails.

        restOfferedItemMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(om.writeValueAsBytes(offeredItem)))
            .andExpect(status().isBadRequest());

        assertSameRepositoryCount(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void getAllOfferedItems() throws Exception {
        // Initialize the database
        offeredItemRepository.saveAndFlush(offeredItem);

        // Get all the offeredItemList
        restOfferedItemMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(offeredItem.getId().intValue())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME)))
            .andExpect(jsonPath("$.[*].stock").value(hasItem(DEFAULT_STOCK)))
            .andExpect(jsonPath("$.[*].price").value(hasItem(sameNumber(DEFAULT_PRICE))))
            .andExpect(jsonPath("$.[*].hidden").value(hasItem(DEFAULT_HIDDEN.booleanValue())))
            .andExpect(jsonPath("$.[*].imageContentType").value(hasItem(DEFAULT_IMAGE_CONTENT_TYPE)))
            .andExpect(jsonPath("$.[*].image").value(hasItem(Base64.getEncoder().encodeToString(DEFAULT_IMAGE))))
            .andExpect(jsonPath("$.[*].limitedStockThreshold").value(hasItem(DEFAULT_LIMITED_STOCK_THRESHOLD)))
            .andExpect(jsonPath("$.[*].category").value(hasItem(DEFAULT_CATEGORY)));
    }

    @Test
    @Transactional
    void getOfferedItem() throws Exception {
        // Initialize the database
        offeredItemRepository.saveAndFlush(offeredItem);

        // Get the offeredItem
        restOfferedItemMockMvc
            .perform(get(ENTITY_API_URL_ID, offeredItem.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(offeredItem.getId().intValue()))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME))
            .andExpect(jsonPath("$.stock").value(DEFAULT_STOCK))
            .andExpect(jsonPath("$.price").value(sameNumber(DEFAULT_PRICE)))
            .andExpect(jsonPath("$.hidden").value(DEFAULT_HIDDEN.booleanValue()))
            .andExpect(jsonPath("$.imageContentType").value(DEFAULT_IMAGE_CONTENT_TYPE))
            .andExpect(jsonPath("$.image").value(Base64.getEncoder().encodeToString(DEFAULT_IMAGE)))
            .andExpect(jsonPath("$.limitedStockThreshold").value(DEFAULT_LIMITED_STOCK_THRESHOLD))
            .andExpect(jsonPath("$.category").value(DEFAULT_CATEGORY));
    }

    @Test
    @Transactional
    void getNonExistingOfferedItem() throws Exception {
        // Get the offeredItem
        restOfferedItemMockMvc.perform(get(ENTITY_API_URL_ID, Long.MAX_VALUE)).andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    void putExistingOfferedItem() throws Exception {
        // Initialize the database
        offeredItemRepository.saveAndFlush(offeredItem);

        long databaseSizeBeforeUpdate = getRepositoryCount();

        // Update the offeredItem
        OfferedItem updatedOfferedItem = offeredItemRepository.findById(offeredItem.getId()).orElseThrow();
        // Disconnect from session so that the updates on updatedOfferedItem are not directly saved in db
        em.detach(updatedOfferedItem);
        updatedOfferedItem
            .name(UPDATED_NAME)
            .stock(UPDATED_STOCK)
            .price(UPDATED_PRICE)
            .hidden(UPDATED_HIDDEN)
            .image(UPDATED_IMAGE)
            .imageContentType(UPDATED_IMAGE_CONTENT_TYPE)
            .limitedStockThreshold(UPDATED_LIMITED_STOCK_THRESHOLD)
            .category(UPDATED_CATEGORY);

        restOfferedItemMockMvc
            .perform(
                put(ENTITY_API_URL_ID, updatedOfferedItem.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(om.writeValueAsBytes(updatedOfferedItem))
            )
            .andExpect(status().isOk());

        // Validate the OfferedItem in the database
        assertSameRepositoryCount(databaseSizeBeforeUpdate);
        assertPersistedOfferedItemToMatchAllProperties(updatedOfferedItem);
    }

    @Test
    @Transactional
    void putNonExistingOfferedItem() throws Exception {
        long databaseSizeBeforeUpdate = getRepositoryCount();
        offeredItem.setId(longCount.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restOfferedItemMockMvc
            .perform(
                put(ENTITY_API_URL_ID, offeredItem.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(om.writeValueAsBytes(offeredItem))
            )
            .andExpect(status().isBadRequest());

        // Validate the OfferedItem in the database
        assertSameRepositoryCount(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithIdMismatchOfferedItem() throws Exception {
        long databaseSizeBeforeUpdate = getRepositoryCount();
        offeredItem.setId(longCount.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restOfferedItemMockMvc
            .perform(
                put(ENTITY_API_URL_ID, longCount.incrementAndGet())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(om.writeValueAsBytes(offeredItem))
            )
            .andExpect(status().isBadRequest());

        // Validate the OfferedItem in the database
        assertSameRepositoryCount(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithMissingIdPathParamOfferedItem() throws Exception {
        long databaseSizeBeforeUpdate = getRepositoryCount();
        offeredItem.setId(longCount.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restOfferedItemMockMvc
            .perform(put(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(om.writeValueAsBytes(offeredItem)))
            .andExpect(status().isMethodNotAllowed());

        // Validate the OfferedItem in the database
        assertSameRepositoryCount(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void partialUpdateOfferedItemWithPatch() throws Exception {
        // Initialize the database
        offeredItemRepository.saveAndFlush(offeredItem);

        long databaseSizeBeforeUpdate = getRepositoryCount();

        // Update the offeredItem using partial update
        OfferedItem partialUpdatedOfferedItem = new OfferedItem();
        partialUpdatedOfferedItem.setId(offeredItem.getId());

        partialUpdatedOfferedItem
            .name(UPDATED_NAME)
            .hidden(UPDATED_HIDDEN)
            .limitedStockThreshold(UPDATED_LIMITED_STOCK_THRESHOLD)
            .category(UPDATED_CATEGORY);

        restOfferedItemMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedOfferedItem.getId())
                    .contentType("application/merge-patch+json")
                    .content(om.writeValueAsBytes(partialUpdatedOfferedItem))
            )
            .andExpect(status().isOk());

        // Validate the OfferedItem in the database

        assertSameRepositoryCount(databaseSizeBeforeUpdate);
        assertOfferedItemUpdatableFieldsEquals(
            createUpdateProxyForBean(partialUpdatedOfferedItem, offeredItem),
            getPersistedOfferedItem(offeredItem)
        );
    }

    @Test
    @Transactional
    void fullUpdateOfferedItemWithPatch() throws Exception {
        // Initialize the database
        offeredItemRepository.saveAndFlush(offeredItem);

        long databaseSizeBeforeUpdate = getRepositoryCount();

        // Update the offeredItem using partial update
        OfferedItem partialUpdatedOfferedItem = new OfferedItem();
        partialUpdatedOfferedItem.setId(offeredItem.getId());

        partialUpdatedOfferedItem
            .name(UPDATED_NAME)
            .stock(UPDATED_STOCK)
            .price(UPDATED_PRICE)
            .hidden(UPDATED_HIDDEN)
            .image(UPDATED_IMAGE)
            .imageContentType(UPDATED_IMAGE_CONTENT_TYPE)
            .limitedStockThreshold(UPDATED_LIMITED_STOCK_THRESHOLD)
            .category(UPDATED_CATEGORY);

        restOfferedItemMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedOfferedItem.getId())
                    .contentType("application/merge-patch+json")
                    .content(om.writeValueAsBytes(partialUpdatedOfferedItem))
            )
            .andExpect(status().isOk());

        // Validate the OfferedItem in the database

        assertSameRepositoryCount(databaseSizeBeforeUpdate);
        assertOfferedItemUpdatableFieldsEquals(partialUpdatedOfferedItem, getPersistedOfferedItem(partialUpdatedOfferedItem));
    }

    @Test
    @Transactional
    void patchNonExistingOfferedItem() throws Exception {
        long databaseSizeBeforeUpdate = getRepositoryCount();
        offeredItem.setId(longCount.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restOfferedItemMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, offeredItem.getId())
                    .contentType("application/merge-patch+json")
                    .content(om.writeValueAsBytes(offeredItem))
            )
            .andExpect(status().isBadRequest());

        // Validate the OfferedItem in the database
        assertSameRepositoryCount(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithIdMismatchOfferedItem() throws Exception {
        long databaseSizeBeforeUpdate = getRepositoryCount();
        offeredItem.setId(longCount.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restOfferedItemMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, longCount.incrementAndGet())
                    .contentType("application/merge-patch+json")
                    .content(om.writeValueAsBytes(offeredItem))
            )
            .andExpect(status().isBadRequest());

        // Validate the OfferedItem in the database
        assertSameRepositoryCount(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithMissingIdPathParamOfferedItem() throws Exception {
        long databaseSizeBeforeUpdate = getRepositoryCount();
        offeredItem.setId(longCount.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restOfferedItemMockMvc
            .perform(patch(ENTITY_API_URL).contentType("application/merge-patch+json").content(om.writeValueAsBytes(offeredItem)))
            .andExpect(status().isMethodNotAllowed());

        // Validate the OfferedItem in the database
        assertSameRepositoryCount(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void deleteOfferedItem() throws Exception {
        // Initialize the database
        offeredItemRepository.saveAndFlush(offeredItem);

        long databaseSizeBeforeDelete = getRepositoryCount();

        // Delete the offeredItem
        restOfferedItemMockMvc
            .perform(delete(ENTITY_API_URL_ID, offeredItem.getId()).accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        assertDecrementedRepositoryCount(databaseSizeBeforeDelete);
    }

    protected long getRepositoryCount() {
        return offeredItemRepository.count();
    }

    protected void assertIncrementedRepositoryCount(long countBefore) {
        assertThat(countBefore + 1).isEqualTo(getRepositoryCount());
    }

    protected void assertDecrementedRepositoryCount(long countBefore) {
        assertThat(countBefore - 1).isEqualTo(getRepositoryCount());
    }

    protected void assertSameRepositoryCount(long countBefore) {
        assertThat(countBefore).isEqualTo(getRepositoryCount());
    }

    protected OfferedItem getPersistedOfferedItem(OfferedItem offeredItem) {
        return offeredItemRepository.findById(offeredItem.getId()).orElseThrow();
    }

    protected void assertPersistedOfferedItemToMatchAllProperties(OfferedItem expectedOfferedItem) {
        assertOfferedItemAllPropertiesEquals(expectedOfferedItem, getPersistedOfferedItem(expectedOfferedItem));
    }

    protected void assertPersistedOfferedItemToMatchUpdatableProperties(OfferedItem expectedOfferedItem) {
        assertOfferedItemAllUpdatablePropertiesEquals(expectedOfferedItem, getPersistedOfferedItem(expectedOfferedItem));
    }
}
