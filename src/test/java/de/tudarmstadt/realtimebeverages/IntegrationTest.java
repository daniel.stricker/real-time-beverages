package de.tudarmstadt.realtimebeverages;

import de.tudarmstadt.realtimebeverages.config.AsyncSyncConfiguration;
import de.tudarmstadt.realtimebeverages.config.EmbeddedSQL;
import de.tudarmstadt.realtimebeverages.config.JacksonConfiguration;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import org.springframework.boot.test.context.SpringBootTest;

/**
 * Base composite annotation for integration tests.
 */
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@SpringBootTest(classes = { RealTimeBeveragesApp.class, JacksonConfiguration.class, AsyncSyncConfiguration.class })
@EmbeddedSQL
public @interface IntegrationTest {
}
